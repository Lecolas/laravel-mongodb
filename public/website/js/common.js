$(function () {
  function toggleBackTop(t) {
    if ($(window).scrollTop() > 100) {
      $('.backTop').addClass('over');
    } else {
      $('.backTop').removeClass('over');
    }
    if ($(window).width() > 750) {
      if ($(window).scrollTop() > 300) {
        $('.header').addClass('over')
      } else {
        $('.header').removeClass('over')
      }
    }
  }
  $('#burger').click(function () {
    $(this).toggleClass('on');
    $('.m-header').toggleClass('over')
    $('.pageBox').toggleClass('out')
  })
  $('.m-header-layer').click(function () {
    $('#burger').removeClass('on');
    $('.m-header').removeClass('over')
    $('.pageBox').removeClass('out')
  })
  // $(window).scroll(function(){
  //     toggleBackTop();
  // })

  // 鎴彇about椤甸潰鐨刪ash

  $('body').addClass('isLoaded')

  $('.openLayVideo').click(function () {
    var ovideo = $(this).data('video');
    var oiframe = $(this).data('iframe')
    if (oiframe) {
      showIframeVideoLayer(oiframe)
    } else {
      showVideoLayer(ovideo)
    }


    return false;
  })


  $(window).scroll(function () {

    if ($(window).scrollTop() > 10) {
      $('.header,.toper').addClass('vblack')
    } else {
      $('.header,.toper').removeClass('vblack')
    }

  })
  $('.fdMenus').each(function () {
    $(this).find('.secNav a:not(.wap-show)').first().trigger('mouseover')
  })
  $('.hh-search a').click(function (e) {
    $('.header .search-box').toggleClass('open')
  })
  $('.header .hh-search,.header .search-box').click(function (e) {
    e.stopPropagation()
  })
  $('body').click(function () {
    $('.header .search-box').removeClass('open')
  })

  $('.product-c-nav dt a').click(function () {
    var odl = $(this).closest('dl')
    $(this).toggleClass('on')
    odl.find('dd').slideToggle()
    odl.siblings('dl').find('dt a').removeClass('on')
    odl.siblings('dl').find('dd').slideUp()
  })
  if (isMobile()) {
    $('.ft-share a, .toper .ashare a,.openWxCode').click(function () {
      var codeImg = $(this).data('code')
      if (codeImg) {
        var desc = $(this).data('description')
        showCodeModel(codeImg, desc)
        return false;
      }
    })
  } else {
    $('.ft-share a, .toper .ashare a,.openWxCode').hover(function () {
      var codeImg = $(this).data('code')
      if (codeImg) {
        var desc = $(this).data('description')
        hoverCodeLayer(codeImg, desc, $(this))
        return false;
      }
    }, function () {
      layer.closeAll()
    })
  }
  pendantFixed($('.fund-hots'), 10)
  pendantFixed($('.his-years'), 60)
  pendantFixed($('.jnews'), 60)
  $(window).scroll()

  $('.ft-helps dt a').click(function (e) {
    $(this).closest('dl').toggleClass('open').siblings().removeClass('open')
    e.stopPropagation()
    return false;
  })
})

function pendantFixed(obj, offtop) {
  if (obj.length && !isMobile()) {
    var rh = obj.offset().top, rl = obj.position().left, rw = obj.outerWidth(true);
    $(document).scroll(function () {
      var objh = obj.outerHeight(true)
      var outTopHeight = $('.toper').height() + $('.header').height()
      var vdis = $(window).scrollTop() > (rh - outTopHeight);
      if (vdis) {
        obj.addClass('widgetFix')
        if ($(window).scrollTop() < ($('.footer').offset().top - objh - outTopHeight - 80)) {
          obj.css({
            top: $(window).scrollTop() - rh + outTopHeight + offtop,
            left: rl
          })
        }
      } else {
        obj.removeClass('widgetFix')
      }
    })
  }
}
// 鍒ゆ柇鏄惁鎵嬫満娴忚鍣�
function isMobile() {
  return $(window).width() <= 750
}

function isIE9() {
  if (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion.split(";")[1].replace(/[ ]/g, "").replace("MSIE", "")) <= 9) {
    return true;
  }
  return false;
}

; (function ($) {

  $.fn.serializeJson = function () {
    var serializeObj = {};
    var array = this.serializeArray();
    var str = this.serialize();
    $(array).each(function () {
      if (serializeObj[this.name]) {
        if ($.isArray(serializeObj[this.name])) {
          serializeObj[this.name].push(this.value);
        } else {
          serializeObj[this.name] = [serializeObj[this.name], this.value];
        }
      } else {
        serializeObj[this.name] = this.value;
      }
    });
    return serializeObj;
  };

  $.fn.customPage = function (options) {
    var config = {
      initPage: 1,
      size: 5,
      prevBtn: '.page-prev',
      nextBtn: '.page-next',
      callback: function () { }
    }
    var opt = $.extend(config, options);
    var size = opt.size;
    var _this = $(this);
    var listObj = $(this).children();
    var totalSize = listObj.length;
    var totalPage = Math.ceil(totalSize / size);
    var prevBtn = $(opt.prevBtn), nextBtn = $(opt.nextBtn);
    var actPage = totalPage ? opt.initPage : 0;

    function changePage() {
      var actList = listObj.slice((actPage - 1) * size, actPage * size)
      _this.html(actList)
      opt.callback({
        actPage: actPage,
        totalPage: totalPage
      })
    }
    changePage();
    _this.show();
    prevBtn.click(function () {
      if (actPage == 1) {
        return false;
      }
      --actPage;
      changePage()
    })
    nextBtn.click(function () {
      if (actPage == totalPage) {
        return false;
      }
      ++actPage;
      console.log(actPage)
      changePage()
    })
  }
})(jQuery);


function CheckUrl(str) {
  var RegUrl = new RegExp();
  RegUrl.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!RegUrl.test(str)) {
    return false;
  }
  return true;
}

// 瑙嗛鎾斁
function showVideoLayer(ohtml) {
  if (!ohtml) {
    layer.msg('Url is error...')
    return false;
  }
  if (CheckUrl(ohtml)) {
    var xshowbox = '<div class="layer-openVideo"><video width="100%" height="100%" src="' + ohtml + '" autoplay controls="controls"></video></div>';
  } else {
    var xshowbox = '<div class="layer-openVideo">' + ohtml + '</div>';
  }

  layer.open({
    type: 1,
    shade: 0.7,
    title: false, //涓嶆樉绀烘爣棰�
    area: [isMobile() ? '90vw' : '800px', isMobile() ? '60vw' : '500px'],
    content: xshowbox,
  })
}
// 澶栭摼瑙嗛鎾斁
function showIframeVideoLayer(ohtml) {
  var oiframe = decodeURIComponent(ohtml)
  var xshowbox = '<div class="layer-openVideo">' + oiframe + '</div>';
  layer.open({
    type: 1,
    shade: 0.7,
    title: false, //涓嶆樉绀烘爣棰�
    area: [isMobile() ? '320px' : '800px'],
    content: xshowbox,
  })
}

function navshover(nav, time) {  //瑙﹀彂鐨勬寜閽紝寮瑰嚭鐨勫唴瀹癸紝寤惰繜鐨勬椂闂�
  var menuShow, menuHide;
  var dtime = time || 100;  //寤惰繜瑙﹀彂鏃堕棿
  var o_nav = $(nav);
  o_nav.hover(function () {
    var _this = $(this);
    var openid = _this.data('menu');
    if (!openid) {
      return;
    }
    clearTimeout(menuHide);
    clearTimeout(menuShow);
    menuShow = setTimeout(function () {
      $("#" + openid).stop().slideDown(150);
      $('.fdMenus').not('#' + openid).hide()
    }, dtime);
  }, function () {
    clearTimeout(menuHide);
    clearTimeout(menuShow);
    var _this = $(this);
    var openid = _this.data('menu');
    menuHide = setTimeout(function () {
      $("#" + openid).hide();
    }, dtime);
  })
  $('.fdMenus').hover(function () {
    clearTimeout(menuHide);
  }, function () {
    var _this = $(this);
    menuHide = setTimeout(function () {
      _this.hide();
    }, dtime);
  })
}
// 鑿滃崟
function navshover2(nav) {  //瑙﹀彂鐨勬寜閽紝寮瑰嚭鐨勫唴瀹癸紝寤惰繜鐨勬椂闂�
  var menuShow, menuHide;
  var o_nav = $(nav);
  o_nav.hover(function () {
    var _this = $(this);
    var wrap = _this.closest('.fdMenus')
    var openid = _this.data('menu');
    _this.addClass('active').siblings().removeClass('active');
    if (openid) {
      $("#" + openid).show().siblings().hide()
    } else {
      wrap.find('.tnavs').hide();
    }
    var aimg = _this.data('img') || 'none',
      atit = _this.data('text') || '',
      ahref = _this.attr('href') || '';

    wrap.find('.secPhoto .img').css({
      'background-image': 'url(' + aimg + ')'
    })
    wrap.find('.secPhoto .tit').text(atit);
    wrap.find('.secPhoto a').attr(ahref)
  }, function () {
  })
}
// 鑿滃崟
function navshover3(nav) {  //瑙﹀彂鐨勬寜閽紝寮瑰嚭鐨勫唴瀹癸紝寤惰繜鐨勬椂闂�
  var menuShow, menuHide;
  var o_nav = $(nav);
  o_nav.click(function () {
    var _this = $(this);
    // var wrap = _this.closest('.fdMenus')
    var openid = _this.data('menu');
    _this.addClass('active').siblings().removeClass('active');
    if (openid) {
      $('.full-menus').show()
      $('#burger').click()
      $("#" + openid).show().siblings().hide()
      $('.fdMenus').not('#' + openid).hide()
      return false;
    }
  })
  $('.full-menus').click(function () {
    $('.fdMenus').hide()
    $('.full-menus').hide()
  })
  $('.fdMenus').click(function (e) {
    e.stopPropagation()
  })
}


// hover寰俊浜岀淮鐮�
function hoverCodeLayer(codeImg, desc, obj) {
  if (!codeImg) {
    return false;
  }
  var xshowbox = '<div style="box-sizing:border-box;text-align:center; font-size:16px; padding:5px 0px;"><img width="120" style="display:block" height="120" src="' + codeImg + '"></div>';
  if (isMobile()) {
    layer.open({
      type: 1,
      shade: 0.7,
      title: false, //涓嶆樉绀烘爣棰�
      area: '140px',
      content: xshowbox,
    })
  } else {
    layer.tips(xshowbox, obj, {
      tips: [1, '#0086EA'],
      time: 20000
    })
  }
}

// 寮瑰嚭浜岀淮鐮�
function showCodeModel(codeImg, desc) {
  if (!codeImg) {
    return false;
  }
  var xshowbox = '<div style="width:180px; text-align:center; font-size:16px; "><img width="180" height="180" style="display:block" src="' + codeImg + '"></div>';

  layer.open({
    type: 1,
    shade: 0.7,
    title: false, //涓嶆樉绀烘爣棰�
    area: [isMobile() ? '180px' : '320px', isMobile() ? '180px' : '400px'],
    content: xshowbox,
  })
}


/**
*	CountUp-function. Tackes the org value -9 (as long as it can) and each digit * 20ms in animation speed
  */
function eleInViewport(a) {
  var b = a.offset().top,
    c = $(window).height(),
    d = $(window).scrollTop()
  return b - d < c
}
function isFloat(n) {
  return Number(n) === n && n % 1 !== 0;
}
function runCounter(_self) {
  var _org = parseFloat(_self.data("org"));
  var _max_steps = 9;
  var _duration_multiplyer = 140;
  var _start_from = 0;
  var _steps = _max_steps;

  if (isFloat(_org)) {
    if (((_org * 10) - _max_steps) > _max_steps) {
      _start_from = _org - (_max_steps / 10);
    }
  }
  else {
    if (_org > _max_steps) {
      _start_from = _org - _max_steps;
    }
    else {
      _steps = _max_steps - (_max_steps - _org);
    }
  }

  var _duration = _steps * _duration_multiplyer;

  _self.prop('Counter', _start_from).animate({
    Counter: _org
  }, {
    duration: _duration,
    easing: 'swing',
    step: function (now) {
      // Check how we should round the data
      var _value = Math.ceil(now);

      var _org = parseFloat($(this).data("org"));

      if (isFloat(_org)) {
        _value = (Math.ceil(now * 10) / 10);

        if (!isFloat(_value)) {
          _value += ".0";
        }
      }

      $(this).text(_value);
    }
  });
}


var TeamDShift = (function () {
  var init = function () {
    $('.team-tracker').on('mousemove', threeDMove);
    $('.team-tracker').on('mouseleave', threeDReset);
  };

  var threeDMove = function (event) {
    var $el = $(this).closest('.team-tracker');
    var parentOffset = $el.offset();
    var relXPos = (event.pageX - parentOffset.left);
    var relYPos = (event.pageY - parentOffset.top);
    var intensity = .1;
    var otherProperty = 'perspective(1200px)';
    var finalX;
    var finalY;

    relXPos = relXPos - ($(this).innerWidth() / 2);
    finalX = relXPos * intensity;
    relYPos = relYPos - ($(this).innerWidth() / 2);
    finalY = relYPos * -intensity;

    $(this).closest('.team-tracker').css({
      'transform': otherProperty + ' rotateY(' + finalX + 'deg) rotateX(' + finalY + 'deg)',
      'transition-delay': '0s',
      'transition': '0.2s'
    });

    shine($el, relXPos, relYPos);
  };

  var threeDReset = function (event) {
    $(this).css({
      'transform': 'rotate3d(0, 0, 0, 0)',
      'transition': '1s'
    });
  };

  var shine = function ($el, x, y) {
    var PI = 3.141592654;
    var arctan = Math.atan2(x, y);      // Totes supposed to pass in Y first, then X but this way give sme proper results 炉\_(銉�)_/炉
    var angle = -arctan * (180 / PI);   // Convert radians -> degrees. Ideally arctan would be positive, but see preceding comment

    $el.find('.team-shine').css(
      'background',
      'linear-gradient(' + angle + 'deg, rgba(255,255,255,0.4) 0%, rgba(255,255,255,0) 100%)'
    )
  }

  return {
    init: init
  };

})();


(function ($) {
  $.fn.numberRock = function (options) {
    var defaults = {
      lastNumber: 100,
      unit: '',
      duration: 2000,
      easing: 'swing'  //swing(榛樿 : 缂撳啿 : 鎱㈠揩鎱�)  linear(鍖€閫熺殑)
    };
    var opts = $.extend({}, defaults, options);

    $(this).animate({
      num: "numberRock",
      // width : 300,
      // height : 300,
    }, {
      duration: opts.duration,
      easing: opts.easing,
      complete: function () {
        console.log("success");
      },
      step: function (a, b) {  //鍙互妫€娴嬫垜浠畾鏃跺櫒鐨勬瘡涓€娆″彉鍖�
        //console.log(a);
        //console.log(b.pos);   //杩愬姩杩囩▼涓殑姣斾緥鍊�(0~1)
        $(this).html(parseInt(b.pos * opts.lastNumber) + opts.unit);
      }
    });

  }

})(jQuery);

$(function () {
  $("[data-count]").each(function () {
    var item = $(this)
    $(window).scroll(function () {
      var vtop = $(window).scrollTop()
      if (!item.data('bool') && (vtop > item.offset().top - $(window).height())) {
        item.data('bool', true)
        var count = item.data('count') + ''
        var num = parseInt(count)
        var unit = count.replace(num + '', '')
        item.numberRock({
          lastNumber: num,
          unit: unit,
          duration: 2000,
          easing: 'swing',  //鎱㈠揩鎱�
        });
      }
    })

  })
  $(window).trigger('scroll')
})