<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>关于我们 - 互联互通</title>
    <link rel="stylesheet" href="{{asset('/website/styles/reset.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/about.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.scss')}}"/>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <link rel="stylesheet" href="{{asset('website/styles/swiper.min.css')}}"/>
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />

</head>
<body>
<!-- Header Start -->
<header class="site-header">
    <div class="wrapper site-header__wrapper">
        <a href="/" class="brand"><img src="{{asset('/website/images/logo.png')}}"/></a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <li class="nav__item"><a href="/">首页</a></li>
                <li class="nav__item dropdown">
                    <a href="{{asset('/nav/product')}}">产品</a>
                    <ul class="sub_ul">
                        <li class="first_child"><a href="{{asset('/detail/22')}}">传输线路</a></li>
                        <li class="nav__item dropdown first_child">
                            <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                            <ul style="    left: 155px;top: 0px; width: 250px">
                                <li class="_sub_nav__item"><a href="/detail/11">力合报业大数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/12">深圳长城大厦数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/13">上海外高桥数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/14">上海（宝山）云数据中心</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav__item dropdown">
                    <a href="/nav/solution">解决方案</a>
                    <ul class="sub_ul" style="width: 230px">
                        <li><a href="/detail/15">银行 保险</a></li>
                        <li><a href="/detail/16">电子商务</a></li>
                        <li><a href="/detail/17">交通物流</a></li>
                        <li><a href="/detail/18">媒体行业</a></li>
                        <li><a href="/detail/19">音频 视频</a></li>
                        <li><a href="/detail/20">游戏行业</a></li>
                        <li><a href="/detail/21">证券 基金</a></li>
                    </ul>
                </li>
                <li class="nav__item"><a href="/news">新闻资讯</a></li>
                <li class="nav__item active"><a href="/about">关于我们</a></li>
            </ul>
        </nav>
    </div>
    <div class="m-toper">
        <div class="wrap clearfix">
            <div class="hh-logo"><a href="/"><img src="{{asset('/website/images/logo.png')}}" alt=""></a></div>
            <div class="burger over" id="burger">
                <div class="burger-in">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-header">
        <div class="m-header-layer"></div>
        <div class="m-header-box">
            <div class="hh-menus">
                <dl class="">
                    <dt><a href="/">首页</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="javascript:; ">产品</a></dt>
                    <dd>
                        <a href="{{url('/detail/22')}}">传输线路</a>
                        <div href="javascript:;" class="hh-menus-child" style="">
                            <dl class="">
                                <dt><a href="javascript:;">IDC机房集群</a></dt>
                                <dd>
                                    <a href="/detail/11">力合报业大数据中心</a>
                                    <a href="/detail/12">深圳长城大厦数据中心</a>
                                    <a href="/detail/13">上海外高桥数据中心</a>
                                    <a href="/detail/14">上海（宝山）云数据中心</a>
                                    <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                                    <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                                </dd>
                            </dl>
                        </div>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="javascript:;">解决方案</a></dt>
                    <dd>
                        <a href="/detail/15">银行 保险</a>
                        <a href="/detail/16">电子商务</a>
                        <a href="/detail/17">交通物流</a>
                        <a href="/detail/18">媒体行业</a>
                        <a href="/detail/19">音频 视频</a>
                        <a href="/detail/20">游戏行业</a>
                        <a href="/detail/21">证券 基金</a>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="/news">新闻资讯</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="/about">关于我们</a></dt>
                </dl>
            </div>
        </div>
    </div>
</header>


<section class="profile" id="introducing">
    <div class="profile-content">
        <h1>公司简介</h1>
        <h2>Company profile</h2>
        <p>
            深圳互联互通数据中心有限公司（以下简称互联互通）成立于2014年，是全国具有重要影响力的网络空间基础设施创新服务提供商，我们的团队均来自于电信系统和世纪互联，拥有平均10多年的行业经验。作为中国具有影响力的互联互通网络平台，互联互通致力于提供业界领先的数据中心服务、高承载传输网络服务，建立了端到端的网络生态系统。
        </p>
        <p>
            互联互通在全国30多个城市运营40多个分布式数据中心，与电信、联通、移动、广电及世纪互联等多家第三方网络服务商建立合作，拥有5000个机柜和100多个网络节点（POP），骨干传输网总长20000km，传输带宽高达160G，具有无可比拟的网络可靠性和稳定性，真正实现全网互联互通。互联互通具有丰富多样的高品质BGP产品，提供高速的互联网及专线接入服务，是国内具有影响力的互联互通平台。依照国家十三五计划提出的“创新、协调、绿色、开放、共享”五大发展理念，互联互通通过颠覆性技术创新、商业模式创新和生态创新，构建高速、移动、安全、泛在的新一代信息基础设施，助力中国“网络强国”战略。
        </p>
        <p>
            互联互通汇聚国内各大运营商、互联网服务提供商、互联网内容提供商等网络资源，建立互联互通的数据港湾，运用完整的跨区域、跨运营商的快速服务体系，为政府、公共事业、金融、电商、游戏、教育、农业、娱乐、医疗、汽车、制造业等行业企业搭建稳定的平台，以更大规模和更高效率为企业带来高附加值的网络空间基础设施服务。
        </p>
    </div>
</section>
<section class="milestone" id="milestone">
    <h1 >企业里程碑</h1>
    <div class="home-story" id="about-history">
        <ul class="swiper-wrapper" style="text-align: left;padding-left: 10px">
            <div class="swiper-slide">
                <div class="t">2020年</div>
                <div class="d">
                    互联互通荣膺2020中国数据中心市场年会“2020年深圳市先进技术”和“云计算中心科技奖（人才奖）”两大奖项，提供5G时代定制
                </div>
            </div>

            <div class="swiper-slide">
                <div class="t">2018年</div>
                <div class="d">
                    互联互通与华为云达成战略合作，成为云+数据中心互联网综合服务商
                </div>
            </div>

            <div class="swiper-slide">
                <div class="t">2017年</div>
                <div class="d">互联互通荣获中国IDC产业“湾区数字化转型基石奖”</div>
            </div>
            <div class="swiper-slide">
                <div class="t">2016年</div>
                <div class="d">光纤管道达2W公里</div>
            </div>
            <div class="swiper-slide">
                <div class="t">2015年</div>
                <div class="d">
                    全国分布式部署节点超过30个，多维度业务成功普及金融行业的全国连锁门店，技术服务逐步辐射全球
                </div>
            </div>
            <div class="swiper-slide">
                <div class="t">2014年</div>
                <div class="d">公司成立，并与各运营商达成战略合作</div>
            </div>
        </ul>
        <div class="swiper-btn swiper-btn-prev">
            <i class="iconfont icon-rightarrow"><</i>
        </div>
        <div class="swiper-btn swiper-btn-next">
            <i class="iconfont icon-rightarrow">></i>
        </div>
    </div>
</section>

<section class="culture">
    <h1 class="page-content" style="color: black">企业文化</h1>

    <div class="page-content">
        <ul class="list">
            <li>
                <h2>使命</h2>
                <p>打造企业云生态，保障客户数据价值</p>
            </li>
            <li>
                <h2>愿景</h2>
                <p>聚焦未来信息发展，实现共同价值</p>
            </li>
            <li>
                <h2>定位</h2>
                <p>专业的数据中心+云信息技术服务提供商</p>
            </li>
            <li>
                <h2>价值观</h2>
                <p>诚信，团队，创新，忠诚，共赢</p>
            </li>
            <li>
                <h2>使命</h2>
                <p>1.提供5G时代定制化数据中心+云综合解决方案和服务</p>
                <p>2.打造可信赖、可延展、可定制的企业云生态</p>
                <p>3.打造可定义、可操作、可视化云数据中心</p>
                <p>4.员工发展与企业使命相结合，提升共同价值</p>
            </li>
        </ul>
    </div>
</section>

<section class="system" id="services">
    <div class="page-content">
        <h1 >服务体系</h1>
        <ul>
            <li class="system-card">
                <header></header>
                <ul>
                    <li>清晰的流程化服务</li>
                    <li>售前咨询-资源查询-工单开通</li>
                    <li>月度计费-运维服务-财务清算</li>
                    <li>客户轻松接入</li>
                    <li>畅快享用服务</li>
                </ul>
            </li>
            <li class="system-card">
                <header></header>
                <ul>
                    <li>完整的服务体系</li>
                    <li>7*24*365不间断客服</li>
                    <li>双NOC全程监控</li>
                    <li>故障处理流程标准化</li>
                    <li>专业技术保障</li>
                </ul>
            </li>
            <li class="system-card">
                <header></header>
                <ul>
                    <li>统一对接商务代表</li>
                    <li>缩短决策链路</li>
                    <li>积极响应与实时沟通</li>
                    <li>以统一视角迎接客户</li>
                    <li>提升客户满意度</li>
                </ul>
            </li>
        </ul>
    </div>
</section>

<div class="footer">
    <div class="ft-center">
        <div class="wrap3 ftc-wrap">
            <div class="ft-helps">
                <dl>
                    <dt><a href="#">首页</a></dt>
                    <dd>
                        <a href="/#our-business">公司业务</a>
                        <a href="/#advantage">业务优势</a>
                        <a href="/#services-advantage">服务优势</a>
                        <a href="/#extra-services">增值服务</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">产品</a></dt>
                    <dd>
                        <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                        <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">解决方案</a></dt>
                    <dd>
                        <a href="/nav/solution">行业方案</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">新闻资讯</a></dt>
                    <dd>
                        <a href="/news?category=8">公司新闻</a>
                        <a href="/news?category=9">行业动态</a>
                        <a href="/news?category=10">媒体报道</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">关于我们</a></dt>
                    <dd>
                        <a href="/about#introducing">公司简介</a>
                        <a href="/about#milestone">企业里程碑</a>
                        <a href="/about#services">服务体系</a>
                    </dd>
                </dl>
            </div>
            <div class="ft-codes">
                <img src="{{asset('/website/images/qr-code.jpg')}}" alt="">
                <span>关注我们</span>
            </div>
        </div>
        <div class="bottomCopyr">
            <div class="wrap3 br-wrap">
                <div class="bcleft">
                    <div class="name">Copyright 2021 深圳互联互通数据中心有限公司 版权所有</div>
                    <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备17133584号-1</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="{{asset('website/js/swiper.min.js')}}"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="{{asset('website/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('website/js/common.js')}}"></script>
<script>
    var pdSwiper = new Swiper("#about-history", {
        slidesPerView: 3.5,
        slidesPerGroup: 1,
        slideToClickedSlide: true,
        speed: 600,
        navigation: {
            prevEl: "#about-history .swiper-btn-prev",
            nextEl: "#about-history .swiper-btn-next",
        },
    });
</script>
</body>
</html>
