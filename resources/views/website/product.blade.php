<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>@if($current == 'product') 产品 @else 解决方案 @endif - 互联互通</title>
    <link rel="stylesheet" href="{{asset('/website/styles/reset.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/product.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.scss')}}"/>
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />

</head>
<body>
<!-- Header Start -->
<header class="site-header">
    <div class="wrapper site-header__wrapper">
        <a href="/" class="brand"><img src="{{asset('/website/images/logo.png')}}"/></a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <li class="nav__item"><a href="/">首页</a></li>
                <li class="nav__item dropdown @if($current == 'product') active @endif">
                    <a href="{{asset('/nav/product')}}">产品</a>
                    <ul class="sub_ul">
                        <li class="first_child"><a href="{{url('/detail/22')}}">传输线路</a></li>
                        <li class="nav__item dropdown first_child">
                            <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                            <ul style="    left: 155px;top: 0px; width: 250px">
                                <li class="_sub_nav__item"><a href="/detail/11">力合报业大数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/12">深圳长城大厦数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/13">上海外高桥数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/14">上海（宝山）云数据中心</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav__item dropdown @if($current == 'solution') active @endif">
                    <a href="/nav/solution">解决方案</a>
                    <ul class="sub_ul" style="width: 230px">
                        <li><a href="/detail/15">银行 保险</a></li>
                        <li><a href="/detail/16">电子商务</a></li>
                        <li><a href="/detail/17">交通物流</a></li>
                        <li><a href="/detail/18">媒体行业</a></li>
                        <li><a href="/detail/19">音频 视频</a></li>
                        <li><a href="/detail/20">游戏行业</a></li>
                        <li><a href="/detail/21">证券 基金</a></li>
                    </ul>
                </li>
                <li class="nav__item"><a href="/news">新闻资讯</a></li>
                <li class="nav__item"><a href="/about">关于我们</a></li>
            </ul>
        </nav>
    </div>
    <div class="m-toper">
        <div class="wrap clearfix">
            <div class="hh-logo"><a href="/"><img src="{{asset('/website/images/logo.png')}}" alt=""></a></div>
            <div class="burger over" id="burger">
                <div class="burger-in">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-header">
        <div class="m-header-layer"></div>
        <div class="m-header-box">
            <div class="hh-menus">
                <dl class="">
                    <dt><a href="/">首页</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="javascript:; ">产品</a></dt>
                    <dd>
                        <a href="{{url('/detail/22')}}">传输线路</a>
                        <div href="javascript:;" class="hh-menus-child" style="">
                            <dl class="">
                                <dt><a href="javascript:;">IDC机房集群</a></dt>
                                <dd>
                                    <a href="/detail/11">力合报业大数据中心</a>
                                    <a href="/detail/12">深圳长城大厦数据中心</a>
                                    <a href="/detail/13">上海外高桥数据中心</a>
                                    <a href="/detail/14">上海（宝山）云数据中心</a>
                                    <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                                    <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                                </dd>
                            </dl>
                        </div>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="javascript:;">解决方案</a></dt>
                    <dd>
                        <a href="/detail/15">银行 保险</a>
                        <a href="/detail/16">电子商务</a>
                        <a href="/detail/17">交通物流</a>
                        <a href="/detail/18">媒体行业</a>
                        <a href="/detail/19">音频 视频</a>
                        <a href="/detail/20">游戏行业</a>
                        <a href="/detail/21">证券 基金</a>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="/news">新闻资讯</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="/about">关于我们</a></dt>
                </dl>
            </div>
        </div>
    </div>
</header>

<section class="cover" style="@if($current == 'solution') background-image: url('/website/images/solution.jpg') @endif">
    <nav class="page-content">
        @if($current == 'product')
        <div>
            <h3>Products</h3>
            <h2>产品</h2>
        </div>
        @elseif ($current == 'solution')
            <div>
                <h3>Solution</h3>
                <h2>解决方案</h2>
            </div>
        @endif
        <div class="breadcrumb">
            <img src="{{asset('/website/images/icon-location.png')}}"/>当前位置：<a href="/"> 首页</a
            >&nbsp;>&nbsp;
            @if($current == 'product')
                <a href="/nav/product"> 产品</a>
            @elseif ($current == 'solution')
                <a href="/nav/solution"> 解决方案</a>
            @endif
        </div>
    </nav>
</section>
<section class="business">
    @if(!empty($list))
        <ul>
            @foreach($list['data'] as $key => $val)
                <li style="cursor: pointer">
                    <a style="height: 340px; width: 384px; background-image: url('{{$val['cover_img']}}'); display: inline-block; background-size: cover;background-position: center" href="{{url('/detail/' . $val['id'])}}"></a>
                    <a style="line-height: 30px;text-align: left;display: block; color: #323131" href="{{url('/detail/' . $val['id'])}}">{{$val['title']}}</a>
                </li>
            @endforeach
        </ul>
        <div class="pagination">
                <div class="pagination-container">
                    @if (!empty($list['links']))
                        @foreach($list['links'] as $key => $val)
                            @if(!is_numeric($val['label']) && preg_match('/Prev/', $val['label']))
                                <a href="{{$list['prev_page_url']}}" class="pagination-prev">
                            <span class="icon-pagination icon-pagination-prev">
                                  <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="-5 -5 24 24"
                                      width="24"
                                      height="24"
                                      preserveAspectRatio="xMinYMin"
                                      class="icon__icon">
                                    <path d="M3.414 7.657l3.95 3.95A1 1 0 0 1 5.95 13.02L.293 7.364a.997.997 0 0 1 0-1.414L5.95.293a1 1 0 1 1 1.414 1.414l-3.95 3.95H13a1 1 0 0 1 0 2H3.414z"></path>
                                  </svg>
                            </span>
                                </a>
                            @elseif (!is_numeric($val['label']) && preg_match('/Next/', $val['label']))
                                <a href="{{$list['next_page_url']}}" class="pagination-next">
                            <span class="icon-pagination icon-pagination-next">
                                  <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="-5 -5 24 24"
                                      width="24"
                                      height="24"
                                      preserveAspectRatio="xMinYMin"
                                      class="icon__icon">
                                    <path d="M3.414 7.657l3.95 3.95A1 1 0 0 1 5.95 13.02L.293 7.364a.997.997 0 0 1 0-1.414L5.95.293a1 1 0 1 1 1.414 1.414l-3.95 3.95H13a1 1 0 0 1 0 2H3.414z"></path>
                                  </svg>
                            </span>
                                </a>
                            @else
                                <a href="{{$val['url']}}" class="pagination-page-number @if($val['active'] == $val['label']) active @endif">{{$val['label']}}</a>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
    @endif
</section>
<div class="footer">
    <div class="ft-center">
        <div class="wrap3 ftc-wrap">
            <div class="ft-helps">
                <dl>
                    <dt><a href="#">首页</a></dt>
                    <dd>
                        <a href="/#our-business">公司业务</a>
                        <a href="/#advantage">业务优势</a>
                        <a href="/#services-advantage">服务优势</a>
                        <a href="/#extra-services">增值服务</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">产品</a></dt>
                    <dd>
                        <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                        <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">解决方案</a></dt>
                    <dd>
                        <a href="/nav/solution">行业方案</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">新闻资讯</a></dt>
                    <dd>
                        <a href="/news?category=8">公司新闻</a>
                        <a href="/news?category=9">行业动态</a>
                        <a href="/news?category=10">媒体报道</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">关于我们</a></dt>
                    <dd>
                        <a href="/about#introducing">公司简介</a>
                        <a href="/about#milestone">企业里程碑</a>
                        <a href="/about#services">服务体系</a>
                    </dd>
                </dl>
            </div>
            <div class="ft-codes">
                <img src="{{asset('/website/images/qr-code.jpg')}}" alt="">
                <span>关注我们</span>
            </div>
        </div>
        <div class="bottomCopyr">
            <div class="wrap3 br-wrap">
                <div class="bcleft">
                    <div class="name">Copyright 2021 深圳互联互通数据中心有限公司 版权所有</div>
                    <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备17133584号-1</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        loop: true,

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
        },
    });
</script>
</body>
</html>
