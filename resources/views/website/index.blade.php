<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>互联互通</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="{{asset('website/styles/reset.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/index.css')}}"/>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <link rel="stylesheet" href="{{asset('website/styles/swiper.min.css')}}"/>
    <!-- <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/> -->

    <link rel="stylesheet" href="{{asset('website/styles/common.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.scss')}}"/>
    <style>
        .swiper-button-next, .swiper-button-prev {
            color: lightgrey !important;
            top: 50%;
        }
    </style>
</head>
<body>
<!-- Header Start -->
<header class="site-header">
    <div class="wrapper site-header__wrapper">
        <a href="/" class="brand"><img src="{{asset('/website/images/logo.png')}}"/></a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <li class="nav__item active"><a href="/">首页</a></li>
                <li class="nav__item dropdown">
                    <a href="{{asset('/nav/product')}}">产品</a>
                    <ul class="sub_ul">
                        <li class="first_child"><a href="{{url('/detail/22')}}">传输线路</a></li>
                        <li class="nav__item dropdown first_child">
                            <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                            <ul style="    left: 155px;top: 0px; width: 250px">
                                <li class="_sub_nav__item"><a href="/detail/11">力合报业大数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/12">深圳长城大厦数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/13">上海外高桥数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/14">上海（宝山）云数据中心</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav__item dropdown">
                    <a href="/nav/solution">解决方案</a>
                    <ul class="sub_ul" style="width: 230px">
                        <li><a href="/detail/15">银行 保险</a></li>
                        <li><a href="/detail/16">电子商务</a></li>
                        <li><a href="/detail/17">交通物流</a></li>
                        <li><a href="/detail/18">媒体行业</a></li>
                        <li><a href="/detail/19">音频 视频</a></li>
                        <li><a href="/detail/20">游戏行业</a></li>
                        <li><a href="/detail/21">证券 基金</a></li>
                    </ul>
                </li>
                <li class="nav__item"><a href="/news">新闻资讯</a></li>
                <li class="nav__item"><a href="/about">关于我们</a></li>
            </ul>
        </nav>
    </div>
    <div class="m-toper">
        <div class="wrap clearfix">
            <div class="hh-logo"><a href="/"><img src="{{asset('/website/images/logo.png')}}" alt=""></a></div>
            <div class="burger over" id="burger">
                <div class="burger-in">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-header">
        <div class="m-header-layer"></div>
        <div class="m-header-box">
            <div class="hh-menus">
                <dl class="">
                    <dt><a href="/">首页</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="javascript:; ">产品</a></dt>
                    <dd>
                        <a href="{{url('/detail/22')}}">传输线路</a>
                        <div href="javascript:;" class="hh-menus-child" style="">
                            <dl class="">
                                <dt><a href="javascript:;">IDC机房集群</a></dt>
                                <dd>
                                    <a href="/detail/11">力合报业大数据中心</a>
                                    <a href="/detail/12">深圳长城大厦数据中心</a>
                                    <a href="/detail/13">上海外高桥数据中心</a>
                                    <a href="/detail/14">上海（宝山）云数据中心</a>
                                    <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                                    <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                                </dd>
                            </dl>
                        </div>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="javascript:;">解决方案</a></dt>
                    <dd>
                        <a href="/detail/15">银行 保险</a>
                        <a href="/detail/16">电子商务</a>
                        <a href="/detail/17">交通物流</a>
                        <a href="/detail/18">媒体行业</a>
                        <a href="/detail/19">音频 视频</a>
                        <a href="/detail/20">游戏行业</a>
                        <a href="/detail/21">证券 基金</a>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="/news">新闻资讯</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="/about">关于我们</a></dt>
                </dl>
            </div>
        </div>
    </div>
</header>
<section class="hero">
    <div class="swiper-container mySwiper">
        <div class="swiper-wrapper">
            @if(!empty($banner))
                @foreach($banner as $key => $val)
                    <div class="swiper-slide">
                        <img src="{{$val['banner_url']}}"/>
                    </div>
                @endforeach
            @endif

        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>
<section class="business" id="our-business">
    <h1>我们的业务</h1>
    <p>秉持“多元化、合作、共赢”的经营理念，致力于成为全球领先的互联网</p>
    <ol>
        <li style="background-image: url('/website/images/index-businesse-3.jpg'); cursor: pointer"
            onclick="window.location.href='/nav/product'">
            <div class="card-content">
                <h3 style="color: white">数据中心</h3>
            </div>
        </li>

        <li style="background-image: url('/website/images/index-businesse-2.jpg'); cursor: pointer"
            onclick="window.location.href='/nav/product'">
            <div class="card-content">
                <h3 style="color: white">网络连接</h3>
            </div>
        </li>

        <li style="background-image: url('/website/images/index-businesse-1.jpg'); cursor: pointer"
            onclick="window.location.href='/nav/product'">
            <div class="card-content">
                <h3 style="color: white">海外业务</h3>
            </div>
        </li>
    </ol>
    <a href="{{url('/nav/product')}}" class="blue-lg-button">全部业务 →</a>
</section>
<section class="advantage" id="advantage">
    <h1>互联互通业务优势</h1>
    <p>
        深圳互联互通自2014年开始大力发展专线业务，经过6年的拓展与经营，已经形成较为丰富的专线运营经验。
        规模在电信增值业务内遥遥领先，业务范围已覆盖国内各个区域。
    </p>
    <div class="advantage-card">
        <ul>
            <li>
                <strong>6年积累，服务领先：</strong>
                <p>从2014年开始发展国际业务以来，已经形成较为丰富的专线运营经验、在网络资源、商务合作、服务保障等各方面均形成了良好的合作体系。</p>
            </li>

            <li>
                <strong>深耕市场，规模领先 ：</strong>
                <p>
                    专线业务是互联互通公司内部最具核心竞争力的产品。累计签约客户近百家，覆盖了互联网、金融证券、交通、教育、大型企业等多个行业。
                </p>
            </li>
            <li>
                <strong>业务直连，覆盖全国 ：</strong>
                <p>
                    互联互通专线业务范围已覆盖全国，主要业务集中在北上广深及二三线城市、已渗透至香港（中国），等区域；
                </p>
            </li>
        </ul>
    </div>
</section>
<section class="service" id="services-advantage">
    <div class="page-content">
        <h1>互联互通服务优势</h1>
        <ul>
            <li>
                <div class="icon-box">
                    <img src="{{asset('website/images/index/advantage-icon-1.jpg')}}"/>
                </div>
                <h3>中立</h3>
                <p>第三方服务商</p>
            </li>
            <li>
                <div class="icon-box">
                    <img src="{{asset('website/images/index/advantage-icon-2.jpg')}}"/>
                </div>
                <h3>高效响应机制</h3>
                <p>7*24在线服务 、5分钟响应 、 现场工程师支持</p>
            </li>
            <li>
                <div class="icon-box">
                    <img src="{{asset('website/images/index/advantage-icon-3.jpg')}}"/>
                </div>
                <h3>覆盖广</h3>
                <p>
                    在国内拥有50+个上门服务点， 百余名现场支撑工程师，
                    能满足客户的布局全国的接入需求
                </p>
            </li>
            <li>
                <div class="icon-box">
                    <img src="{{asset('website/images/index/advantage-icon-4.jpg')}}"/>
                </div>
                <h3>质量保证</h3>
                <p>所有项目均按时测试验收，保证交付</p>
            </li>
        </ul>
    </div>
</section>
<section class="value-added" id="extra-services">
    <h1>增值服务</h1>
    <ul>
        <li class="value-added-card">
            <header>通信工程建设服务</header>
            <ul>
                <li>光缆工程建设</li>
                <li>设备安装、拆卸</li>
                <li>机房设备上架、上电、配置、 调试、交付</li>
                <li>机房网络设备、路由器等物理 迁移</li>
            </ul>
        </li>
        <li class="value-added-card">
            <header>网络维护服务</header>
            <ul>
                <li>长途干线光缆维护</li>
                <li>机房机房定期巡检、基础设施维护</li>
                <li>设备连纤、网线跳接、端口标签、 模块安装、更换板卡、紧急排障</li>
            </ul>
        </li>
        <li class="value-added-card">
            <header>系统集成服务</header>
            <ul>
                <li>设备代采购</li>
                <li>设备上门安装</li>
                <li>设备现场/远程调试</li>
                <li>设备备件库管理</li>
                <li>备件代运维管理</li>
            </ul>
        </li>
    </ul>
</section>
<div class="footer">
    <div class="ft-center">
        <div class="wrap3 ftc-wrap">
            <div class="ft-helps">
                <dl>
                    <dt><a href="#">首页</a></dt>
                    <dd>
                        <a href="/#our-business">公司业务</a>
                        <a href="/#advantage">业务优势</a>
                        <a href="/#services-advantage">服务优势</a>
                        <a href="/#extra-services">增值服务</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">产品</a></dt>
                    <dd>
                        <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                        <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">解决方案</a></dt>
                    <dd>
                        <a href="/nav/solution">行业方案</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">新闻资讯</a></dt>
                    <dd>
                        <a href="/news?category=8">公司新闻</a>
                        <a href="/news?category=9">行业动态</a>
                        <a href="/news?category=10">媒体报道</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">关于我们</a></dt>
                    <dd>
                        <a href="/about#introducing">公司简介</a>
                        <a href="/about#milestone">企业里程碑</a>
                        <a href="/about#services">服务体系</a>
                    </dd>
                </dl>
            </div>
            <div class="ft-codes">
                <img src="{{asset('/website/images/qr-code.jpg')}}" alt="">
                <span>关注我们</span>
            </div>
        </div>
        <div class="bottomCopyr">
            <div class="wrap3 br-wrap">
                <div class="bcleft">
                    <div class="name">Copyright 2021 深圳互联互通数据中心有限公司 版权所有</div>
                    <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备17133584号-1</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Swiper JS -->
<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
<script src="{{asset('website/js/swiper.min.js')}}"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="{{asset('website/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('website/js/common.js')}}"></script>
<script>
    AOS.init({
        once: true,
    });
</script>
<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper(".mySwiper", {
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
</script>
</body>
</html>
