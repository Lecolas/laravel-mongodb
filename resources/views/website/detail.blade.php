
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>{{$detail['title']}}  - 互联互通</title>
    <link rel="stylesheet" href="{{asset('/website/styles/reset.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/article.css')}}"/>
    <link rel="stylesheet" href="{{asset('/website/styles/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('website/styles/common.css')}}"/>
    <link rel="stylesheet" type="stylesheet/scss" href="{{asset('website/styles/common.scss')}}"/>
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
    <style>
        .nav__item a {
            color: white;
            text-decoration: none;
        }

        .page-header {
            min-width: var(--content-width);
            margin-top: 0;
            padding: 0 0 32px 0;
        }

        .page-header nav {
            color: #a59d9d; !important;
            font-size: 14px; !important;
            line-height: 15px;
            margin: 15px;
        }

        .horizon {
            display: block;
            width: 100%;
            height: 1px;
            border-top: 1px solid lightgray;
            margin-bottom: 66px;
            margin-top: 10px;
        }

        .article-info span {
            margin-right: 2rem;
        }

        #back-to-top {
            background-image: url("/website/images/backToTop.png");
            display: block;
            height: 100px;
            width: 100px;
            position: fixed;
            right: 1px;
            background-repeat: no-repeat;
            background-position: center;
            cursor: pointer;
            bottom: 5rem;
            display: none;
        }
    </style>
</head>
<body>

<!-- Header Start -->
<header class="site-header dark">
    <div class="wrapper site-header__wrapper">
        <a href="/" class="brand"><img src="{{asset('/website/images/logo.png')}}"/></a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <li class="nav__item active"><a href="/">首页</a></li>
                <li class="nav__item dropdown">
                    <a href="{{asset('/nav/product')}}">产品</a>
                    <ul class="sub_ul">
                        <li class="first_child"><a href="{{asset('/detail/22')}}">传输线路</a></li>
                        <li class="nav__item dropdown first_child">
                            <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                            <ul style="    left: 155px;top: 0px; width: 250px">
                                <li class="_sub_nav__item"><a href="/detail/11">力合报业大数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/12">深圳长城大厦数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/13">上海外高桥数据中心</a></li>
                                <li class="_sub_nav__item"><a href="/detail/14">上海（宝山）云数据中心</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav__item dropdown">
                    <a href="/nav/solution">解决方案</a>
                    <ul class="sub_ul" style="width: 230px">
                        <li><a href="/detail/15">银行 保险</a></li>
                        <li><a href="/detail/16">电子商务</a></li>
                        <li><a href="/detail/17">交通物流</a></li>
                        <li><a href="/detail/18">媒体行业</a></li>
                        <li><a href="/detail/19">音频 视频</a></li>
                        <li><a href="/detail/20">游戏行业</a></li>
                        <li><a href="/detail/21">证券 基金</a></li>
                    </ul>
                </li>
                <li class="nav__item"><a href="/news">新闻资讯</a></li>
                <li class="nav__item"><a href="/about">关于我们</a></li>
            </ul>
        </nav>
    </div>
    <div class="m-toper">
        <div class="wrap clearfix">
            <div class="hh-logo"><a href="/"><img src="{{asset('/website/images/logo.png')}}" alt=""></a></div>
            <div class="burger over" id="burger">
                <div class="burger-in">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-header">
        <div class="m-header-layer"></div>
        <div class="m-header-box">
            <div class="hh-menus">
                <dl class="">
                    <dt><a href="/">首页</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="javascript:; ">产品</a></dt>
                    <dd>
                        <a href="{{url('/detail/22')}}">传输线路</a>
                        <div href="javascript:;" class="hh-menus-child" style="">
                            <dl class="">
                                <dt><a href="javascript:;">IDC机房集群</a></dt>
                                <dd>
                                    <a href="/detail/11">力合报业大数据中心</a>
                                    <a href="/detail/12">深圳长城大厦数据中心</a>
                                    <a href="/detail/13">上海外高桥数据中心</a>
                                    <a href="/detail/14">上海（宝山）云数据中心</a>
                                    <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                                    <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                                </dd>
                            </dl>
                        </div>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="javascript:;">解决方案</a></dt>
                    <dd>
                        <a href="/detail/15">银行 保险</a>
                        <a href="/detail/16">电子商务</a>
                        <a href="/detail/17">交通物流</a>
                        <a href="/detail/18">媒体行业</a>
                        <a href="/detail/19">音频 视频</a>
                        <a href="/detail/20">游戏行业</a>
                        <a href="/detail/21">证券 基金</a>
                    </dd>
                </dl>
                <dl class="">
                    <dt><a href="/news">新闻资讯</a></dt>
                </dl>
                <dl class="">
                    <dt><a href="/about">关于我们</a></dt>
                </dl>
            </div>
        </div>
    </div>
</header>



<div class="page-header page-content" style="min-width: var(--content-width); margin-top: 0">
    <nav>当前位置：首页 ⊙ {{$detail['category']['category_name']}} ⊙ {{$detail['title']}}</nav>
    <div class="horizon"></div>

    <h1>{{$detail['title']}}</h1>
    <div class="article-info">
        <span>来源：{{$detail['source_from']}}</span>
{{--        <span>作者：</span>--}}
        <span>发布时间：{{$detail['created_at']}}</span>
        <span>{{$detail['view_sum']}}次浏览</span>
    </div>
</div>

<div class="article-content">
    <div class="page-content">
        {!! htmlspecialchars_decode($detail['blog_content']) !!}
    </div>
    <div class="pagination page-content" style="display: none">
        <div class="pagination-prev">
            <a href="/">上一篇：能源行业解决方案</a>
        </div>
        <div class="pagination-next"><a href="/">下一篇：销售/市场</a></div>
    </div>
</div>

<a id="back-to-top"></a>

<div class="footer">
    <div class="ft-center">
        <div class="wrap3 ftc-wrap">
            <div class="ft-helps">
                <dl>
                    <dt><a href="#">首页</a></dt>
                    <dd>
                        <a href="/#our-business">公司业务</a>
                        <a href="/#advantage">业务优势</a>
                        <a href="/#services-advantage">服务优势</a>
                        <a href="/#extra-services">增值服务</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">产品</a></dt>
                    <dd>
                        <a href="{{asset('/nav/product?cid=11')}}">传输线路</a>
                        <a href="{{asset('/nav/product?cid=13')}}">IDC机房集群</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">解决方案</a></dt>
                    <dd>
                        <a href="/nav/solution">行业方案</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">新闻资讯</a></dt>
                    <dd>
                        <a href="/news?category=8">公司新闻</a>
                        <a href="/news?category=9">行业动态</a>
                        <a href="/news?category=10">媒体报道</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="#">关于我们</a></dt>
                    <dd>
                        <a href="/about#introducing">公司简介</a>
                        <a href="/about#milestone">企业里程碑</a>
                        <a href="/about#services">服务体系</a>
                    </dd>
                </dl>
            </div>
            <div class="ft-codes">
                <img src="{{asset('/website/images/qr-code.jpg')}}" alt="">
                <span>关注我们</span>
            </div>
        </div>
        <div class="bottomCopyr">
            <div class="wrap3 br-wrap">
                <div class="bcleft">
                    <div class="name">Copyright 2021 深圳互联互通数据中心有限公司 版权所有</div>
                    <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备17133584号-1</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Swiper JS -->
<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>

<script src="{{asset('website/js/swiper.min.js')}}"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="{{asset('website/js/common.js')}}"></script>

<script>
    AOS.init({
        once: true,
    });
</script>
<script type="application/javascript">
    $(window).scroll(function(){
        var scrollTop = $(this).scrollTop();
        var scrollHeight = $(document).height();
        var windowHeight = $(this).height();

        if (scrollTop > 100) {
            $("#back-to-top").show();
        } else {
            $("#back-to-top").height()
        }

        let percentTage = (scrollTop + windowHeight) / scrollHeight;

        if(percentTage >=0.9){
            $("#back-to-top").css('bottom', '400px');
        } else if (percentTage <= 0.2) {
            $("#back-to-top").hide();
        }else {
            $("#back-to-top").css('bottom', '5rem');
        }
    });

    $(function(){
        if (!!window.ActiveXObject || "ActiveXObject" in window) {
            var imgEle = $(".article-content .page-content").height();
            if (imgEle > 700) {
                $('body').css("display", "table-cell");
            }
        }

    });

    $("#back-to-top").click(function () {
        $('html,body').animate({"scrollTop": 0}, 800);
    })
</script>
</body>
</html>
