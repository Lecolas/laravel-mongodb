@extends('layout.master')

@section('pageTitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">文章管理</h1>
            <p class="description">在这里管理您的所有文章</p>
        </div>
    </div>
@endsection

@section('mainContent')
    <div class="col-sm-12" x-data="blog()" x-init="[loadBlogs(), loadCategory()]">
        <div class="panel panel-default" id="pageInfo" >
            <div class="panel-heading">
                <form role="form" class="form-inline">

                    <div class="form-group col-3">
                        <label class="ontrol-label">文章标题:</label>
                        <input type="text" class="form-control" name="title" size="25" placeholder=""
                               x-model="title"/>
                    </div>

                    <div class="form-group col-3">
                        <label class="ontrol-label">文章分类:</label>
                        <script type="text/javascript">
                            jQuery(document).ready(function($)
                            {
                                $("#s2example-1").select2({
                                    placeholder: '选择文章分类',
                                    allowClear: true
                                }).on('select2-open', function()
                                {
                                    // Adding Custom Scrollbar
                                    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                });

                            });
                        </script>
                        <select class="form-control" x-model="category_id" style="width: 300px">
                            <option></option>
                            <template x-for="[index, value] of Object.entries(category)" :key="index">
                                <option :value="value.id" x-text="value.category_name"></option>
                            </template>

                        </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-secondary btn-single" type="button"
                                id="doSearch" x-on:click="loadBlogs()">搜索</button>
                        <button class="btn btn-white btn-single"
                                onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"
                                type="button">新增文章</button>
                    </div>
                </form>
            </div>
            <div class="panel-body">



                <table class="table table-bordered table-striped" id="example-2">
                    <thead>
                    <tr>
                        <th class="no-sorting">
                            <input type="checkbox" class="cbr">
                        </th>
                        <th>文章名称</th>
                        <th>文章分类</th>
                        <th>文章封面</th>
                        <th>文章简介</th>
                        <th>文章状态</th>
                        <th>创建时间</th>
                        <th>更新时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>

                    <tbody class="middle-align">

                    <template style="display: none" x-for="[index, value] of Object.entries(categoryData)" :key="index">
                        <tr>
                            <td>
                                <input type="checkbox" class="cbr">
                            </td>
                            <td x-text="value.title"></td>
                            <td x-text="value.category.category_name"></td>
                            <td x-text="value.cover_img"></td>
                            <td x-text="value.blog_description"></td>
                            <td x-text="value.status"></td>
                            <td x-text="value.created_at"></td>
                            <td x-text="value.updated_at"></td>
                            <td>
                                <a href="#" class="btn btn-secondary btn-sm btn-icon icon-left"
                                   x-on:click="showEdit(value)">
                                    编辑
                                </a>

                                <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"
                                   x-on:click="deleteOne(value.id)">
                                    删除
                                </a>
                            </td>
                        </tr>
                    </template>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{asset('/assets/js/datatables/dataTables.bootstrap.css')}}">
    <script src="{{asset('/assets/js/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script>
        function blog() {
            return {
                title: '',
                category_id: '',
                listRaw: {},
                categoryData: [],
                category: [],
                page: 1,
                loadBlogs() {
                    let searchKeys = {title: this.title, category_id: this.category_id};
                    fetch('{{url('/admin/blog/list')}}', {
                        body: JSON.stringify(searchKeys),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                this.categoryData = [];
                                alert(categoryRes.msg);
                                return false;
                            }
                            this.categoryData = categoryRes.data.data;
                            this.listRaw = categoryRes;
                        });
                },
                loadCategory: function() {
                    fetch('{{url('/admin/blog/getCategory')}}', {
                        body: JSON.stringify({}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(res => {
                            if (res.code != 200) {
                                alert(res.msg);
                                return false;
                            }
                            this.category = res.data;
                        });
                },
                deleteOne: function(id) {
                    console.log(id)
                    fetch('{{url('blog/deleteOne')}}', {
                        body: JSON.stringify({id: id}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                alert(categoryRes.msg);
                                return false;
                            }
                            alert(categoryRes.msg);
                            $("#doSearch").click();
                        });
                },
                showEdit: function(row) {
                    jQuery('#modal-6').modal('show', {backdrop: 'static'});
                    document.getElementById('modelEditId').value = row.id
                    window.reloadDataForm(row.id);
                    let event = new Event('custom-event');
                    window.dispatchEvent(event);
                }

            }
        }
    </script>
@endsection

@section('modals')
    <link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
    <script src="{{asset('assets/js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/js/ckeditor/adapters/jquery.js')}}"></script>
    <script src="{{asset('assets/js/select2/select2.min.js')}}"></script>

    <div class="modal fade" id="modal-6" x-data="editModal()" x-ref="editData"
         data-backdrop="static" @custom-event.window="loadData()" x-init="loadCategory()">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal Content is Responsive</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <input type="hidden" x-model="id" id="modelEditId"/>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="field-1">文章标题</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" x-model="rowData.title" placeholder="请输入文章标题">
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="field-1">文章来源</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" x-model="rowData.source_from" placeholder="请输入文章标题">
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="field-1">文章状态</label>

                                <div class="col-sm-10">
                                    <p>
                                        <label class="cbr-inline">
                                            <input type="radio" name="radio-2" x-model="rowData.status"
                                                   class="cbr" checked value="1">
                                            立刻发布
                                        </label>
                                        <label class="cbr-inline">
                                            <input type="radio" name="radio-2" class="cbr"
                                                x-model="rowData.status" value="2">
                                            存为草稿
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="field-1">文章分类</label>

                                <div class="col-sm-10">
                                    <script type="text/javascript">
                                        jQuery(document).ready(function($)
                                        {
                                            $("#s2example-1").select2({
                                                placeholder: '选择文章分类',
                                                allowClear: true
                                            }).on('select2-open', function()
                                            {
                                                // Adding Custom Scrollbar
                                                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                            });

                                        });
                                    </script>
                                    <select class="form-control" x-model="rowData.category_id">
                                        <option></option>
                                        <template x-for="[index, value] of Object.entries(category)" :key="index">
                                            <option :value="value.id" x-text="value.category_name"></option>
                                        </template>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">文章简介</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" x-model="rowData.blog_description" placeholder="请输入文章简介">
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">文章封面</label>

                                <div class="col-sm-10">
                                    <button class="btn btn-gray" type="button" @click="callUpload()">上传文件</button>
                                    <input style="display: none" type="file" class="form-control" id="uploadFile"
                                           accept="image/x-png,image/gif,image/jpeg,image/bmp" x-on:change="doUpload($event.target)">
                                    <img width="200px" x-bind:src="rowData.cover_img" />
                                </div>
                            </div>
                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">文章内容</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="10" name="DSC"
                                              x-model="rowData.blog_content"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-info" x-on:click="submitForm()">保存</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        let ckEditor = CKEDITOR.replace('DSC', {
            filebrowserImageUploadUrl :"/admin/blog/contentImgUpload",
            removePlugins:'elementspath,resize',
            removeDialogTabs: 'image:advanced;image:Link',
        });
        let _id = '';
        function editModal() {
            return {
                id: '',
                rowData: {
                    title: '',
                    status: '',
                    category_id: '',
                    blog_description: '',
                    blog_content: '',
                    source_from: '',
                    cover_img: '',
                },
                category: {},
                loadData: function () {
                    console.log(_id)
                    if (!_id) {
                        return false;
                    }

                    fetch('{{url('/admin/blog/getOne')}}', {
                        body: JSON.stringify({id: _id}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(res => {
                            if (res.code != 200) {
                                alert(res.msg);
                                return false;
                            }
                            this.rowData = res.data;
                            ckEditor.setData(res.data.blog_content);
                            $("input[name=radio-2][value='" + res.data.status +"']").click()
                        });
                },
                loadCategory: function() {
                    fetch('{{url('/admin/blog/getCategory')}}', {
                        body: JSON.stringify({}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(res => {
                            if (res.code != 200) {
                                alert(res.msg);
                                return false;
                            }
                            this.category = res.data;
                        });
                },
                callUpload: function() {
                    $('#uploadFile').click();
                },
                doUpload: function($event) {
                    let formData = new FormData();
                    formData.append('cover', $event.files[0]);
                    fetch('{{url('/admin/blog/img/upload')}}', {
                        method: 'POST',
                        body: formData,
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                    }).then(response => response.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => {
                            console.log('Success:', response);
                            this.rowData.cover_img = response.data
                        });
                },
                submitForm: function () {
                    let requestData = this.rowData;
                    requestData['id'] = _id;
                    requestData['blog_content'] = CKEDITOR.instances['DSC'].getData();
                    if (requestData['title'] == '') {
                        alert('请输入文章标题');
                        return false;
                    }
                    if (requestData['source_from'] == '') {
                        alert('请输入文章来源');
                        return false;
                    }
                    if (requestData['category_id'] == '') {
                        alert('请选择文章分类');
                        return false;
                    }
                    if (requestData['blog_description'] == '') {
                        alert('请输入文章简介');
                        return false;
                    }
                    if (requestData['cover_img'] == '') {
                        alert('请上传文章封面');
                        return false;
                    }
                    fetch('{{url('/admin/blog/edit')}}', {
                        body: JSON.stringify(requestData),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(res => {
                            if (res.code != 200) {
                                alert(res.msg);
                                return false;
                            }
                            alert(res.msg);
                            location.reload();
                        });
                }
            }
        }

        function reloadDataForm(id) {
            _id = id;
        }
    </script>
@endsection
