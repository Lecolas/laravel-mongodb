@extends('layout.master')

@section('pageTitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">文章分类管理</h1>
            <p class="description">在这里管理你的博客文章分类</p>
        </div>
    </div>
@endsection

@section('mainContent')
    <div class="col-sm-12" x-data="category()" x-init="loadCategory()">
        <div class="panel panel-default" id="pageInfo" >
            <div class="panel-heading">
                <form role="form" class="form-inline">

                    <div class="form-group">
                        <label class="ontrol-label">分类名称:</label>
                        <input type="text" class="form-control" name="category_name" size="25" placeholder=""
                               x-model="searchKeys.category_name"/>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-secondary btn-single" type="button"
                                id="doSearch" x-on:click="loadCategory()">搜索</button>
                        <button class="btn btn-white btn-single"
                                onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"
                                type="button">新增分类</button>
                    </div>
                </form>
            </div>
            <div class="panel-body">

                <table class="table table-bordered table-striped" id="example-2">
                    <thead>
                    <tr>
                        <th class="no-sorting">
                            <input type="checkbox" class="cbr">
                        </th>
                        <th>分类名称</th>
                        <th>更新时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>

                    <tbody class="middle-align">

                    <template x-for="[index, value] of Object.entries(categoryData)" :key="index">
                        <tr>
                            <td>
                                <input type="checkbox" class="cbr">
                            </td>
                            <td x-text="value.category_name"></td>
                            <td x-text="value.updated_at"></td>
                            <td>
                                <a href="#" class="btn btn-secondary btn-sm btn-icon icon-left"
                                   x-on:click="showEdit(value)">
                                    编辑
                                </a>

                                <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"
                                   x-on:click="deleteOne(value.id)">
                                    删除
                                </a>
                            </td>
                        </tr>
                    </template>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{asset('assets/js/datatables/dataTables.bootstrap.css')}}">
    <script src="{{asset('assets/js/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script>
        function category() {
            return {
                listRaw: {},
                categoryData: [],
                page: 1,
                searchKeys: {
                    'category_name': ''
                },
                loadCategory() {
                    let responseData;
                    let searchKeys = {category_name: this.searchKeys.category_name};
                    fetch('{{url('/admin/blog/category/list')}}', {
                        body: JSON.stringify(searchKeys),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                this.categoryData = [];
                                alert(categoryRes.msg);
                                return false;
                            }
                            this.categoryData = categoryRes.data.data;
                            this.listRaw = categoryRes;
                        });
                },
                deleteOne: function(id) {
                    console.log(id)
                    fetch('{{url('/admin/blog/category/deleteOne')}}', {
                        body: JSON.stringify({id: id}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                alert(categoryRes.msg);
                                return false;
                            }
                            alert(categoryRes.msg);
                            $("#doSearch").click();
                        });
                },
                showEdit: function(row) {
                    jQuery('#modal-6').modal('show', {backdrop: 'static'});
                    window.refill(row)
                }

            }
        }
    </script>
@endsection

@section('modals')
    <div class="modal fade" id="modal-6" x-data="editModal()">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal Content is Responsive</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" id="rowId" x-model="rowId" value="">
                            <div class="form-group">
                                <label for="field-1" class="control-label">分类名称</label>

                                <input type="text" class="form-control" id="category_input" x-model="categoryName" placeholder="请输入分类名称">
                            </div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-info" x-on:click="saveChange()">保存</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        let row_id = '';
        function editModal() {
            return {
                rowId: row_id,
                categoryName: '',
                saveChange: function() {
                    if ('' == this.categoryName) {
                        alert('请输入分类名称');
                        return false;
                    }

                    let requestData = {
                        id: row_id,
                        categoryName: this.categoryName
                    }

                    fetch('{{url('/admin/blog/category/edit')}}', {
                        body: JSON.stringify(requestData),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                alert(categoryRes.msg);
                                return false;
                            }
                            this.rowId = '';
                            this.categoryName = '';
                            $(".closeModal").click();
                            alert(categoryRes.msg);
                            $("#doSearch").click();
                        });
                },
            }
        }

        function refill(row) {
            $("#category_input").val(row.category_name);
            row_id = row.id
        }
    </script>
@endsection
