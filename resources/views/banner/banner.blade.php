@extends('layout.master')

@section('pageTitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">首页轮播图管理</h1>
            <p class="description">在这里管理您的首页轮播图</p>
        </div>
    </div>
@endsection

@section('mainContent')
    <div class="col-sm-12" x-data="blog()" x-init="[loadBlogs()]">
        <div class="panel panel-default" id="pageInfo" >
            <div class="panel-heading">
                <form role="form" class="form-inline">
                    <div class="form-group">
                        <input type="file" style="display: none" id="upload" x-on:change="uploadBanner($event.target)">
                        <button class="btn btn-white btn-single"
                                onclick="$('#upload').click();"
                                type="button">新增banner图</button>
                    </div>
                </form>
            </div>
            <div class="panel-body">



                <table class="table table-bordered table-striped" id="example-2">
                    <thead>
                    <tr>
                        <th>banner</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>

                    <tbody class="middle-align">

                    <template style="display: none" x-for="[index, value] of Object.entries(categoryData)" :key="index">
                        <tr>
                            <td><img x-bind:src="value.banner_url" width="320px"/></td>
                            <td x-text="value.created_at"></td>
                            <td>
                                <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"
                                   x-on:click="deleteOne(value.id)">
                                    删除
                                </a>
                            </td>
                        </tr>
                    </template>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{asset('/assets/js/datatables/dataTables.bootstrap.css')}}">
    <script src="{{asset('/assets/js/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script>
        function blog() {
            return {
                title: '',
                category_id: '',
                listRaw: {},
                categoryData: [],
                category: [],
                page: 1,
                loadBlogs() {
                    let searchKeys = {title: this.title, category_id: this.category_id};
                    fetch('{{url('/admin/banner/list')}}', {
                        body: JSON.stringify(searchKeys),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                this.categoryData = [];
                                alert(categoryRes.msg);
                                return false;
                            }
                            this.categoryData = categoryRes.data.data;
                            console.log(categoryRes.data.data)
                            this.listRaw = categoryRes;
                        });
                },
                deleteOne: function(id) {
                    console.log(id);
                    fetch('{{url('/admin/banner/deleteOne')}}', {
                        body: JSON.stringify({id: id}),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(categoryRes => {
                            if (categoryRes.code != 200) {
                                alert(categoryRes.msg);
                                return false;
                            }
                            alert(categoryRes.msg);
                            window.location.reload()
                        });
                },
                uploadBanner: function($event) {
                    let formData = new FormData();
                    formData.append('banner', $event.files[0]);
                    fetch('{{url('/admin/banner/upload')}}', {
                        method: 'POST',
                        body: formData,
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                    }).then(response => response.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => {
                            alert('上传成功');
                            window.location.reload()
                        });
                },

            }
        }
    </script>
@endsection

