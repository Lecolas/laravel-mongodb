<div class="settings-pane">

    <a href="#" data-toggle="settings-pane" data-animate="true">
        &times;
    </a>

    <div class="settings-pane-inner">

        <div class="row">

            <div class="col-md-4">

                <div class="user-info">

                    <div class="user-image">
                        <a href="#">
                            <input type="file" style="display: none" id="uploadAvatar" />
                            <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->avatar)}}"
                                 class="img-responsive img-circle" id="userAvatarImg" style="width: 130px; height: 130px"/>
                        </a>
                    </div>

                    <div class="user-details">

                        <h3>
                            <a href="#">{{\Illuminate\Support\Facades\Auth::user()->admin_name}}</a>

                            <!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
                            <span class="user-status is-online"></span>
                        </h3>

                        <p class="user-title">Web Master</p>

                        <div class="user-links">
                            <a href="#" class="btn btn-primary" onclick="$('#uploadAvatar').click()">更换头像</a>
                            <a href="#" class="btn btn-success" onclick="jQuery('#resetPassword').modal('show', {backdrop: 'static'});">设置密码</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-8 link-blocks-env">

                <div class="links-block left-sep" style="display: none">
                    <h4>
                        <span>Notifications</span>
                    </h4>

                    <ul class="list-unstyled">
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
                            <label for="sp-chk1">Messages</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
                            <label for="sp-chk2">Events</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
                            <label for="sp-chk3">Updates</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
                            <label for="sp-chk4">Server Uptime</label>
                        </li>
                    </ul>
                </div>

                <div class="links-block left-sep">
                    <h4>
                        <a href="#">
                            <span>登录信息</span>
                        </a>
                    </h4>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                上次登录时间： {{\Illuminate\Support\Facades\Auth::user()->last_login_time}}
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                上次登录IP： {{\Illuminate\Support\Facades\Auth::user()->last_login_ip}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>

    <script>
        $("#uploadAvatar").change(function () {
            let file = $(this).prop('files');
            let formData = new FormData();
            formData.append('avatar', file[0]);
            fetch('{{url('/admin/uploadAvatar')}}', {
                method: 'POST',
                body: formData,
                headers: {
                    "X-CSRF-TOKEN": "{{csrf_token()}}"
                },
            }).then(response => response.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    alert(response.msg);
                    $("#userAvatarImg").attr('src', response.data)
                });
        });
    </script>

</div>
