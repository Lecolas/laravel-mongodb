<!-- Bottom Scripts -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/TweenMax.min.js')}}"></script>
<script src="{{asset('assets/js/resizeable.js')}}"></script>
<script src="{{asset('assets/js/joinable.js')}}"></script>
<script src="{{asset('assets/js/xenon-api.js')}}"></script>
<script src="{{asset('assets/js/xenon-toggles.js')}}"></script>


<!-- Imported scripts on this page -->
<script src="{{asset('assets/js/xenon-widgets.js')}}"></script>
<script src="{{asset('assets/js/devexpress-web-14.1/js/globalize.min.js')}}"></script>
<script src="{{asset('assets/js/devexpress-web-14.1/js/dx.chartjs.js')}}"></script>
<script src="{{asset('assets/js/toastr/toastr.min.js')}}"></script>


<!-- JavaScripts initializations and stuff -->
<script src="{{asset('assets/js/xenon-custom.js')}}"></script>

<div class="modal fade" id="resetPassword">
    <div class="modal-dialog" x-data="resetPassword()">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">修改密码</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="">旧密码</label>

                            <div class="col-sm-10">
                                <input type="password" class="form-control" x-model="old_password"
                                       placeholder="请输入旧密码">
                            </div>
                        </div>
                        <div class="form-group-separator"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="">新密码</label>

                            <div class="col-sm-10">
                                <input type="password" class="form-control" x-model="password"
                                       placeholder="请输入新密码">
                            </div>
                        </div>
                        <div class="form-group-separator"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="">确认新密码</label>

                            <div class="col-sm-10">
                                <input type="password" class="form-control" x-model="passwordConfirmed"
                                       placeholder="请再次输入新密码">
                            </div>
                        </div>
                        <div class="form-group-separator"></div>
                    </form>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-info" x-on:click="doReset()">保存</button>
            </div>
        </div>
    </div>

    <script>
        function resetPassword() {
            return {
                old_password: '',
                password: '',
                passwordConfirmed: '',
                doReset: function () {
                    let requestData = {
                        old_password: this.old_password,
                        password: this.password,
                        password_confirmed: this.passwordConfirmed
                    };
                    fetch('/admin/resetPassword', {
                        body: JSON.stringify(requestData),
                        headers: {
                            "X-CSRF-TOKEN": "{{csrf_token()}}"
                        },
                        method: "post"
                    }).then(res => res.json())
                        .then(res => {
                            if (res.code != 200) {
                                alert(res.msg);
                                return false;
                            }
                            window.location.href = '/logout';
                        });
                }
            }
        }
    </script>
</div>
