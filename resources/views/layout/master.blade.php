@include('layout/header')
<body class="page-body">
@include('layout.userInfo')
<div class="page-container">
    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
    <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
    <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
    <div class="sidebar-menu toggle-others fixed">

        @include('layout/sideBar')

    </div>

    <div class="main-content">

        <!-- User Info, Notifications and Menu Bar -->
        @include('layout/topBar')

        @hasSection('pageTitle')
            @yield('pageTitle')
        @endif

        <div class="row">
            @section('mainContent')
            @show
        </div>


    </div>

</div>


<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
@hasSection('modals')
    @yield('modals')
@endif
@include('layout/bottomScript')

</body>
</html>
