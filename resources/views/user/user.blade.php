@extends('layout.master')

@section('pageTitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">文章管理</h1>
            <p class="description">在这里管理您的所有文章</p>
        </div>
    </div>
@endsection

@section('mainContent')
    <link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
    <script src="{{asset('assets/js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/js/ckeditor/adapters/jquery.js')}}"></script>
    <script src="{{asset('assets/js/select2/select2.min.js')}}"></script>

    <div class="col-sm-12" x-data="category()" x-init="loadCategory()">
        <div class="panel panel-default" id="pageInfo">
            <div class="row">
                <form role="form" class="form-horizontal">
                    <input type="hidden" x-model="id" id="modelEditId"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="field-1">文章标题</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" x-model="rowData.title" placeholder="请输入文章标题">
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="field-1">文章来源</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" x-model="rowData.source_from" placeholder="请输入文章标题">
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="field-1">文章状态</label>

                        <div class="col-sm-10">
                            <p>
                                <label class="cbr-inline">
                                    <input type="radio" name="radio-2" x-model="rowData.status"
                                           class="cbr" checked value="1">
                                    立刻发布
                                </label>
                                <label class="cbr-inline">
                                    <input type="radio" name="radio-2" class="cbr"
                                           x-model="rowData.status" value="2">
                                    存为草稿
                                </label>
                            </p>
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="field-1">文章分类</label>

                        <div class="col-sm-10">
                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    $("#s2example-1").select2({
                                        placeholder: '选择文章分类',
                                        allowClear: true
                                    }).on('select2-open', function () {
                                        // Adding Custom Scrollbar
                                        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                    });

                                });
                            </script>
                            <select class="form-control" x-model="rowData.category_id">
                                <option></option>
                                <template x-for="[index, value] of Object.entries(category)" :key="index">
                                    <option :value="value.id" x-text="value.category_name"></option>
                                </template>

                            </select>
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="">文章简介</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" x-model="rowData.blog_description"
                                   placeholder="请输入文章简介">
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="">文章封面</label>

                        <div class="col-sm-10">
                            <button class="btn btn-gray" type="button" @click="callUpload()">上传文件</button>
                            <input style="display: none" type="file" class="form-control" id="uploadFile"
                                   accept="image/x-png,image/gif,image/jpeg,image/bmp"
                                   x-on:change="doUpload($event.target)">
                            <img width="200px" x-bind:src="rowData.cover_img"/>
                        </div>
                    </div>
                    <div class="form-group-separator"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="">文章内容</label>

                        <div class="col-sm-10">
                                    <textarea class="form-control" rows="10" name="DSC"
                                              x-model="rowData.blog_content"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        let ckEditor = CKEDITOR.replace('DSC', {
            filebrowserImageUploadUrl: "/blog/contentImgUpload",
            removePlugins: 'elementspath,resize',
            removeDialogTabs: 'image:advanced;image:Link',
        });
        let _id = '';

        function editModal() {
            return {
                rowData: {
                    admin_name: {{}},
                    role_id: '',
                    last_login_ip: '',
                    last_login_time: '',
                    phone_no: '',
                    email: '',
                    is_freeze: '',
                },
            }
        }

        function reloadDataForm(id) {
            _id = id;
        }
    </script>
@endsection
