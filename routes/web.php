<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Blog;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Banner\Banner;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Website\Website::class, 'index']);

Route::get('/index', [App\Http\Controllers\Website\Website::class, 'index']);

Route::get('/about', function () {
    return view('website.about');
});

Route::get('/news',  [App\Http\Controllers\Website\Website::class, 'getNews']);
Route::get('/nav/{target}',  [App\Http\Controllers\Website\Website::class, 'getBlogList']);
Route::get('/detail/{id}', [App\Http\Controllers\Website\Website::class, 'detail']);


Route::get('/login', function () {
    return view('login');
});
Route::post('/doLogin', [LoginController::class, 'login']);
Route::post('/menu', [App\Http\Controllers\Menu\MenuController::class, 'getMenu']);


Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
    Route::get('/', [\App\Http\Controllers\Index::class, 'index']);
    Route::get('/logout', [LoginController::class, 'logout']);
    Route::get('/test', function () {
        var_dump(\Illuminate\Support\Facades\Auth::user());
    });

    Route::get('/blog', function () {
        return view('blog.blog');
    });
    Route::get('/productBlog', function () {
        return view('blog.productBlog');
    });
    Route::get('/news', function () {
        return view('blog.news');
    });
    Route::get('/solution', function () {
        return view('blog.solution');
    });

    Route::get('/blog/category', function () {
        return view('blog.blogCategory');
    });

    Route::post('/admin/uploadAvatar', [AdminController::class, 'editAvatar']);
    Route::post('/admin/resetPassword', [AdminController::class, 'changePassword']);

    Route::post('/blog/edit', [Blog\BlogController::class, 'edit']);
    Route::post('/blog/list', [Blog\BlogController::class, 'getList']);
    Route::post('/blog/getOne', [Blog\BlogController::class, 'getOne']);
    Route::post('/blog/deleteOne', [Blog\BlogController::class, 'deleteOne']);
    Route::post('/blog/setIndexShow', [Blog\BlogController::class, 'setIndexShow']);
    Route::post('/blog/category/edit', [Blog\BlogCategoryController::class, 'edit']);
    Route::post('blog/category/list', [Blog\BlogCategoryController::class, 'getList']);
    Route::post('/blog/getCategory', [Blog\BlogCategoryController::class, 'getCategory']);
    Route::post('/blog/category/deleteOne', [Blog\BlogCategoryController::class, 'deleteOne']);
    Route::post('/blog/img/upload', [Blog\BlogController::class, 'imgUpload']);
    Route::post('/blog/contentImgUpload', [Blog\BlogController::class, 'contentImgUpload']);

    Route::get('/banner', function () {
        return view('banner.banner');
    });
    Route::post('/banner/list', [Banner::class, 'getList']);
    Route::post('/banner/deleteOne', [Banner::class, 'deleteOne']);
    Route::post('/banner/upload', [Banner::class, 'imgUpload']);
});
