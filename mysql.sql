-- MariaDB dump 10.19  Distrib 10.4.18-MariaDB, for Win64 (AMD64)
--
-- Host: 47.96.129.67    Database: official_website
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `source_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `show_index` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` (`id`, `title`, `blog_description`, `content_file`, `cover_img`, `view_sum`, `status`, `source_from`, `show_index`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'反倒是给发我我喷雾器热破','啊氛围隔热一个福袋不规范的富士达','反倒是给发我我喷雾器热破.blog','http://loc.test.cc:8080/storage/imgs/60fe4a10b7611.gif',0,2,'按时风刀霜剑啊了',0,4,'2021-07-26 05:38:06','2021-07-26 06:56:14',NULL),(2,'反倒是给发我我喷雾器热破','啊氛围隔热一个福袋不规范的富士达','反倒是给发我我喷雾器热破.blog','http://loc.test.cc:8080/storage/imgs/60fe4a10b7611.gif',0,1,'按时风刀霜剑啊了',0,4,'2021-07-26 06:41:26','2021-07-26 07:09:16','2021-07-26 07:09:16'),(3,' 盛夏假日清新穿搭🍃轻松穿出优雅时髦感',' 盛夏假日清新穿搭🍃轻松穿出优雅时髦感',' 盛夏假日清新穿搭🍃轻松穿出优雅时髦感.blog','http://loc.test.cc:8080/storage/imgs/60ff9a9dedba8.jpg',0,1,'小红书',1,1,'2021-07-27 13:33:29','2021-08-13 16:26:44',NULL),(4,'测试测试啦啦啦啦','范德萨解放路肯德基萨拉看房间里哭到死','测试测试啦啦啦啦.blog','http://loc.test.cc:8080/storage/imgs/6114ca8f42dd3.gif',0,1,'不告诉你',1,6,'2021-08-12 15:15:32','2021-08-13 16:27:34',NULL),(5,'特斯拉“model 2”爆料：售价 15万元，中国设计制造','特斯拉“model 2”爆料：售价 15万元，中国设计制造','特斯拉“model 2”爆料：售价 15万元，中国设计制造.blog','http://www.idcd.cc/storage/imgs/6115116048b09.jpg',0,1,'垃圾自媒体',0,7,'2021-08-12 20:20:06','2021-08-12 20:20:06',NULL);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-13 16:47:56
