<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    use HasFactory;

    protected $table = 'Membership';
    protected $fillable = ['member_name', 'phone', 'email', 'points', 'member_level',
        'valid_date', 'created_at', 'updated_at'];
}
