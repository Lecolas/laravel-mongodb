<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'administrator';
    public $fillable = ['id', 'admin_name', 'admin_password', 'role_id', 'last_login_time',
        'phone_no', 'email', 'is_freeze', 'last_login_ip', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = 'U';
}
