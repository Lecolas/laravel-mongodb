<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nette\Utils\DateTime;

class Blog extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'blog';
    protected $fillable = [
        'title', 'blog_description', 'content_file', 'cover_img',
        'view_sum', 'status', 'source_from', 'category_id', 'created_at',
        'updated_at', 'deleted_at', 'show_index'
    ];
    public $dateFormat = 'Y-m-d H:i:s';

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function getStatusAttribute($status)
    {
        return $status == 1 ? '已发布' : '草稿';
    }

    public function category()
    {
        return $this->hasOne(BlogCategory::class, 'id', 'category_id')->select(['id', 'category_name']);
    }
}
