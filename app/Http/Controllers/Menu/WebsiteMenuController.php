<?php

namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;

class WebsiteMenuController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Menu();
    }

    public function edit(Request $request)
    {
        $id = $request->post('id');
        $only = ['pid', 'menu_name'];
        $form = $request->only($only);

        $parentNodeExist = $this->model->where('id', $form['pid'])->find();
        if (empty($parentNodeExist)) {
            return $this->jsonReturn(201, '不存在的父级菜单');
        }

        if (!empty($id)) {
            $row = $this->model->where('id', $id)->find();
            if (empty($row)) {
                return $this->jsonReturn(201, '不存在的菜单');
            }
            $row->pid = $form['pid'];
            $row->menu_name = $form['menu_name'];
            $row->level = $parentNodeExist->level + 1;
            $result = $row->save();
        } else {
            $form['level'] = $parentNodeExist->level + 1;
            $result = $this->model->create($form);
        }

        if (!$result) {
            return $this->jsonReturn(201, '保存失败');
        }
        return $this->jsonReturn(200, '保存成功');
    }

    public function deleteOne(Request $request)
    {
        $id = $request->post('id');

        $inUse = $this->model->where('pid', $id)->get();
        if (empty($inUse)) {
            return $this->jsonReturn(201, '该菜单还有子菜单，不能删除');
        }

        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败');
        }
        return $this->jsonReturn(200, '删除成功');
    }

}
