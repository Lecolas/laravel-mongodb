<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Menu;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function edit(Request $request)
    {
        $id = $request->post('id');
        $only = ['role_name', 'pid', 'access_menu'];
        $form = $request->post($only);
        $accessMenu = explode(',', $form['access_menu']);
        $menu = Menu::whereIn('id', $accessMenu)->get()->toArray();
        if (count($menu) != count($accessMenu)) {
            return $this->jsonReturn(201, '检测到不存在的菜单');
        }
        if (empty($id)) {
            $result = $this->model->create($form);
        } else {
            $result = $this->model->where('id', $id)->update($form);
        }
        if (empty($result)) {
            return $this->jsonReturn(201, '保存失败');
        }
        return $this->jsonReturn(200, '保存成功');
    }

    public function getList(Request $request)
    {
        $page = $request->post('page', 1);
        $limit = $request->post('limit', 15);
        $list = $this->model->orderBy('id')->paginate($limit, ['*'], 'page', $page);
        return $this->jsonReturn(200, 'succeed', $list);
    }

    public function deleteOne(Request $request)
    {
        $id = $request->post('id');
        $inUse = Admin::where('role_id', $id)->find();
        if (!empty($inUse)) {
            return $this->jsonReturn(201, '该角色正在使用中，不能被删除');
        }
        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败！');
        }
        return $this->jsonReturn(200, '删除成功！');
    }
}
