<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login(Request $request)
    {

        $credentials = $request->only(['email', 'password']);

        if(Auth::guard('web')->attempt($credentials)) {

            $list = Menu::where('level', 1)->with('subMenu')->get()->toArray();
            \session(['userMenu' => $list]);
            return response()->json(['code' => 200, 'msg' => '登录成功']);
        }else{
            return response()->json(['code' => 201, 'msg' => '登录失败，用户名或密码错误']);
        }
    }

    public function logout(Request $request)
    {
        $user = Auth::user();
        $user->last_login_time = date('Y-m-d H:i:s');
        $user->last_login_ip = $request->getClientIp();
        $user->save();
        Auth::logout();
        return redirect('/login');
    }
}
