<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;

class AdminController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Admin();
    }

    public function getList(Request $request)
    {
        $search = [
            'admin_name' => 'fuzzy',
            'role_id' => 'normal',
            'phone_no' => 'normal',
            'email' => 'normal',
            'is_freeze' => 'normal'
        ];

        $only = array_keys($search);
        $form = $request->only($only);
        $page = $request->post('page');
        $limit = $request->post('limit');
        $whereClause = [];

        foreach ($search as $key => $val) {
            if (isset($form[$key]) && !empty($form[$key])) {
                if ($val == 'fuzzy') {
                    $whereClause[$key] = ['like', "%{$form[$key]}%"];
                } else {
                    $whereClause[$key] = $form[$key];
                }
            }
        }

        $data = $this->model->where($whereClause)->orderBy('id', 'asc')
            ->paginate($limit, ['*'], 'page', $page)->toArray();
        return response()->json(['code' => 200, 'msg' => 'succeed', 'data' => $data]);
    }

    public function edit(Request $request)
    {
        $id = $request->post('id');
        $fields = [
            'admin_name' => 'required',

            'role_id' => 'required',
            'phone_no' => 'required',
        ];
        $validation = [
            'admin_name.required' => '用户名不能为空',
            'role_id.required' => '角色不能为空',
            'phone_no' => '联系方式不能为空',
        ];

        if (!empty($id)) {
            $fields['admin_password'] = 'required|min:6';
            $fields['admin_password_confirmed'] = 'required';
            $validation['admin_password.min'] = '密码长度最小6位数';
            $validation['admin_password.required'] = '密码不能为空';
            $validation['admin_password_confirmed.required'] = '请确认密码';
        }

        $only = array_keys($fields);
        $form = $request->only($only);
        $validated = Validator::make($request, $fields, $validation);
        if ($validated->fails()) {
            return response()->json([
                'code' => 201,
                'msg' => $validated->errors()->first(),
                'data' => []
            ]);
        }

        if (empty($id)) {
            if ($form['admin_password'] != $form['admin_password_confirmed']) {
                return $this->jsonReturn(201, '两次密码输入不一致');
            }
            $form['admin_password'] = Hash::make($form['admin_password']);
            $result = $this->model->create($form);
        } else {
            $result = $this->where('id', $id)->update($form);
        }

        if (!$result) {
            return $this->jsonReturn(201, '创建用户失败，请重试');
        }
        return $this->jsonReturn(200, '创建成功！');
    }

    public function deleteOne(Request $request)
    {
        $id = $request->post('id');
        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败');
        }
        return $this->jsonReturn(200, '操作成功！');
    }

    public function freezeUser(Request $request)
    {
        $id = $request->post('id');
        $row = $this->model->find($id);
        if (!$row) {
            return $this->jsonReturn(201, '不存在的用户');
        }
        $row->is_freeze = 1;
        $result = $row->save();
        if (!$result) {
            return $this->jsonReturn(201, '冻结失败！');
        }
        return $this->jsonReturn(200, '操作成功！');
    }

    public function setRole(Request $request)
    {
        $id = $request->post('id');
        $roleId = $request->post('role_id');

        $row = $this->model->find($id);
        if (empty($row)) {
            return $this->jsonReturn(201, '不存在的用户');
        }

        $roleId = Role::find($roleId);
        if (empty($roleId)) {
            return $this->jsonReturn(201, '不存在的角色信息');
        }

        $row->role_id = $roleId;
        $result = $row->save();
        if (!$result) {
            return $this->jsonReturn(201, '角色设置失败！');
        }
        return $this->jsonReturn(200, '角色设置成功！');
    }

    public function changePassword(Request $request)
    {
        $form = [
            'password' => $request->json('password'),
            'old_password' => $request->json('old_password'),
            'password_confirmation' => $request->json('password_confirmed')
        ];
        $validation = Validator::make($form, [
            'password' => 'required|confirmed|min:6',
            'old_password' => 'required'
        ], [
            'password.required' => '请输入密码',
            'password.confirmed' => '请再次输入密码',
            'password.min' => '密码长度最小6位',
            'old_password.required' => '请输入旧密码'
        ]);
        if ($validation->fails()) {
            return $this->jsonReturn(201, $validation->errors()->first());
        }

        $user = Auth::user();

        $passwordCheck = Hash::check($form['old_password'], $user->password);

        if (!$passwordCheck) {
            return $this->jsonReturn(201, '密码错误！');
        }
        $user->password = Hash::make($form['password']);
        $result = $user->save();
        if (!$result) {
            return $this->jsonReturn(201, '修改密码失败');
        }
        return $this->jsonReturn(200, '修改密码成功，请重新登陆！');
    }

    public function editAvatar(Request $request)
    {
        $file = $request->file('avatar');
        $originalFileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFileName = uniqid('avatar') . '.' .$extension;
        $originalFilePath = $file->getRealPath();
        $result = Storage::disk('avatar')->put($newFileName, file_get_contents($originalFilePath));
        if (!$result) {
            return $this->jsonReturn(201, '上传失败');
        }
        $path = 'storage/avatar/' . $newFileName;
        $user = Auth::user();
        $user->avatar = $path;
        $result = $user->save();
        if (!$result) {
            return $this->jsonReturn(201, '更新失败');
        }
        return $this->jsonReturn(200, '更新成功', asset($path));
    }
}
