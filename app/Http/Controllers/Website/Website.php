<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Website extends Controller
{
    protected $menu = [
        'product' => ['category_id' => 13, 'view_page' => 'website.product'],
        'solution' => ['category_id' => 6, 'view_page' => 'website.product'],
        'news' => ['category_id' => 7, 'view_page' => 'website.product'],
    ];

    public function index()
    {
        $list = Blog::query()->where(['show_index' => 1])->get()->toArray();
        $banner = Banner::query()->get()->toArray();
        return view('website.index')->with(['list' => $list, 'banner' => $banner]);
    }

    public function getBlogList(Request $request, $target)
    {
        $page = $request->get('page');
        $instance = Blog::query();
        $menu = $this->menu[strtolower($target)] ?? [];
        if (empty($menu)) {
            redirect(404);
        }
        $cid = $request->get('cid');
        if (empty($cid) && $menu['category_id'] == 1) {
            $allId = BlogCategory::query()->where('category_pid', 1)->get()->toArray();
            $allId = array_column($allId, 'id');
            $instance->whereIn('category_id', $allId);
        } elseif (!empty($cid)) {
            $instance->where(['category_id' => $cid]);
        } else {
            $instance->where('category_id', $menu['category_id']);
        }
        $list = $instance->paginate(6, ['*'], 'page', $page);
        if (!empty($list)) {
            $list = $list->toArray();
        }

        return view($menu['view_page'])->with(['list' => $list, 'current' => $target]);
    }

    public function getNews(Request $request)
    {
        $page = $request->get('page');
        $category = $request->get('category', 8);

        $list = Blog::query()->where('category_id', $category)->paginate('15', '*', 'page', $page);
        if (!empty($list)) {
            $list = $list->appends(['category' => $category])->toArray();
        }
        return view('website.news')->with(['list' => $list, 'cate' => $category]);
    }

    public function detail(Request $request, $id)
    {
        $row = Blog::query()->with('category')->where('status', 1)->find($id);
        if (empty($row)) {
            return redirect(404);
        }
        if (!Storage::disk('blog')->exists($row['content_file'])) {
            return redirect(404);
        }

        Blog::query()->with('category')->where('status', 1)
            ->where('id', $id)->increment('view_sum');

        $row['blog_content'] = Storage::disk('blog')->get($row['content_file']);
        $row['blog_content'] = base64_decode($row['blog_content']);

        return view('website.detail')->with(['detail' => $row]);
    }
}
