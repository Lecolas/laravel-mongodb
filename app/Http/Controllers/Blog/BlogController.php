<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Blog();
    }

    public function getList(Request $request)
    {
        $whereClause = [];
        $page = $request->json('page');
        $title = $request->json('title');
        $limit = $request->json('limit');
        $category_id = $request->json('category_id');
        $category_pid = $request->json('category_pid');

        if (!empty($title)) {
            $whereClause[] = ['title', 'like', "%{$title}%"];
        }

        if (!empty($category_id)) {
            $whereClause['category_id'] = $category_id;
        }

        $obj = $this->model->query()->where($whereClause)->with('category');

        if (!empty($category_pid)) {
            $category_ids = BlogCategory::query()->where('category_pid', $category_pid)->select('id')->get()->toArray();
            $category_ids = array_column($category_ids, 'id');
            $obj->whereIn('category_id', $category_ids);
        }

        $result = $obj->orderBy('updated_at', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
        return $this->jsonReturn(200, 'succeed', $result);
    }

    public function getOne(Request $request)
    {
        $id = $request->json('id');
        $result = $this->model->find($id);
        if (empty($result)) {
            return $this->jsonReturn(201, '不存在的文章');
        }

        if (!Storage::disk('blog')->exists($result['content_file'])) {
            return $this->jsonReturn(201, '文章详情丢失');
        }
        $result['blog_content'] = Storage::disk('blog')->get($result['content_file']);
        $result['blog_content'] = base64_decode($result['blog_content']);

        return $this->jsonReturn(200, 'succeed', $result);
    }

    public function edit(Request $request)
    {
        $form = [];
        $id = $request->json('id');
        $only = ['title', 'blog_description', 'cover_img', 'status', 'blog_content', 'source_from', 'category_id'];
        foreach ($only as $key => $val) {
            $form[$val] = $request->json($val);
        }

        $form['content_file'] = $form['title'] . '.blog';
        $encrypt_content = base64_encode($form['blog_content']);

        if (empty($id)) {
            $form['content_file'] = $form['title'] . '.blog';
            $writeResult = Storage::disk('blog')->put($form['content_file'], $encrypt_content);
            if (!$writeResult) {
                return $this->jsonReturn(201, '保存失败');
            }
            $result = $this->model->create($form);
        } else {
            $row = $this->model->find($id);
            if (empty($row)) {
                return $this->jsonReturn(201, '不存在的文章');
            }
            Storage::disk('blog')->delete($form['content_file']);
            Storage::disk('blog')->put($form['content_file'], $encrypt_content);

            unset($form['blog_content']);
            $result = $this->model->where('id', $id)->update($form);
        }

        if (!$result) {
            return $this->jsonReturn(201, '保存失败');
        }
        return $this->jsonReturn(200, '保存成功');
    }

    public function deleteOne(Request $request)
    {
        $id = $request->json('id');
        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败');
        }
        return $this->jsonReturn(200, '删除成功！');
    }

    public function changeStatus(Request $request)
    {
        $id = $request->post('id');
        $status = $request->post('status');

        $row = $this->model->where('id', $id)->find();
        if (empty($row)) {
            return $this->jsonReturn(201, '不存在的文章');
        }

        $row->status = $status;
        $result = $row->save();
        if (!$result) {
            return $this->jsonReturn(201, '保存成功！');
        }
        return $this->jsonReturn(200, '保存成功！');
    }

    public function imgUpload(Request $request)
    {
        $file = $request->file('cover');
        $ext = $file->getClientOriginalExtension();
        $path = $file->getRealPath();
        $fileName = uniqid() . '.' . $ext;
        $result = Storage::disk('img')->put($fileName, file_get_contents($path));
        if (!$result) {
            return $this->jsonReturn(201, '上传失败');
        }

        $filePath = asset('storage/imgs/' . $fileName);
        return $this->jsonReturn(200, 'succeed', $filePath);
    }

    public function contentImgUpload(Request $request)
    {
        $CKEditorFuncNum = $request->get('CKEditorFuncNum');
        $file = $request->file('upload');
        $ext = $file->getClientOriginalExtension();
        $originalName = $file->getClientOriginalName();
        $path = $file->getRealPath();
        $fileName = uniqid() . '.' . $ext;
        $result = Storage::disk('img')->put($fileName, file_get_contents($path));
        if (!$result) {
            return $this->jsonReturn(201, '上传失败');
        }

        $filePath = asset('storage/imgs/' . $fileName);
        $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '{$filePath}', '')</script>";
        @header('Content-type: text/html; charset=utf-8');
        echo $re;
    }

    public function setIndexShow(Request $request)
    {
        $id = $request->json('id');

        $row = Blog::query()->find($id);
        if (empty($row)) {
            return $this->jsonReturn(201, '不存在的文章');
        }

        $maxBlog = Blog::query()->where('show_index', 1)->count();
        if ($maxBlog >= 3 && $row->show_index == 0) {
            return $this->jsonReturn(201, '首页最多可展示3条文章（不区分类型）');
        }

        if ($row->show_index == 0) {
            $row->show_index = 1;
        } else {
            $row->show_index = 0;
        }
        if (!$row->save()) {
            return $this->jsonReturn(201, '设置失败');
        }
        return $this->jsonReturn(200, '设置成功');
    }

}
