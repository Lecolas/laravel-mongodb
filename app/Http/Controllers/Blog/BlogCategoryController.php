<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new BlogCategory();
    }

    public function getList(Request $request)
    {
        $whereClause = [];
        $page = $request->json('page', 1);
        $limit = $request->json('limit', 15);
        $category_name = $request->json('category_name');

        if (!empty($category_name)) {
            $whereClause[] = ['category_name', 'like', "%{$category_name}%"];
        }
        $result = $this->model->where($whereClause)->orderBy('updated_at', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
        return $this->jsonReturn(200, 'succeed', $result);
    }

    public function edit(Request $request)
    {
        $id = $request->json('id');
        $category_name = $request->json('categoryName');
        if (empty($id)) {
            $result = $this->model->create(['category_name' => $category_name]);
        } else {
            $row = $this->model->find($id);
            if (empty($row)) {
                return $this->jsonReturn(201, '不存在的分类');
            }
            $row->category_name = $category_name;
            $result = $row->save();
        }
        if (!$result) {
            return $this->jsonReturn(201, '保存失败');
        }
        return $this->jsonReturn(200, '保存成功');
    }

    public function deleteOne(Request $request)
    {
        $id = $request->json('id');
        $inUse = Blog::where('category_id', $id)->first();
        if (!empty($inUse)) {
            return $this->jsonReturn(201, '删除失败，该分类下存在文章');
        }
        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败');
        }
        return $this->jsonReturn(200, '删除成功！');
    }

    public function getCategory(Request $request)
    {
        $list = $this->model->get();
        return $this->jsonReturn(200, 'succeed', $list);
    }
}
