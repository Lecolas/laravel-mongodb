<?php

namespace App\Http\Controllers\Customer;

use App\Facades\MongoLogFacade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class CustomerController extends Controller
{
    public function getList(Request $request)
    {
        $redis = Redis::connection();
        $redis->sadd('settingTest', 1);
        $redis->sadd('settingTest', 2);
        print_r($redis->spop('settingTest'));
        print_r($redis->smembers('settingTest'));
    }

    public function test()
    {
        $log_arr = [
            'level' => 'error',
            'trace' => '/App/Https/Controllers/Customer/CustomerController::20',
            'created_at' => date('Y-m-d H:i:s'),
            'message' => 'here is a error message',
            'code_line' => 1
        ];
        $result = MongoLogFacade::insert($log_arr);
        var_dump($result);
    }

    public function search()
    {
        $where = [];
        $result = MongoLogFacade::where($where)->get();
        print_r($result);
        die;
    }

    public function delete()
    {
        var_dump(MongoLogFacade::delete());
    }
}
