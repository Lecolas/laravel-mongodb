<?php

namespace App\Http\Controllers\Banner;

use App\Http\Controllers\Controller;
use App\Models\Banner as BannerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Banner extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new BannerModel();
    }

    public function getList(Request $request)
    {
        $page = $request->json('page');
        $limit = $request->json('limit');

        $result = $this->model->paginate($limit, ['*'], 'page', $page);
        return $this->jsonReturn(200, 'succeed', $result);
    }

    public function edit(Request $request)
    {
        $form = [];
        $id = $request->json('id');
        $only = ['title', 'blog_description', 'cover_img', 'status', 'blog_content', 'source_from', 'category_id'];
        foreach ($only as $key => $val) {
            $form[$val] = $request->json($val);
        }

        $form['content_file'] = $form['title'] . '.blog';
        $encrypt_content = base64_encode($form['blog_content']);

        if (empty($id)) {
            $form['content_file'] = $form['title'] . '.blog';
            $writeResult = Storage::disk('blog')->put($form['content_file'], $encrypt_content);
            if (!$writeResult) {
                return $this->jsonReturn(201, '保存失败');
            }
            $result = $this->model->create($form);
        } else {
            $row = $this->model->find($id);
            if (empty($row)) {
                return $this->jsonReturn(201, '不存在的文章');
            }
            Storage::disk('blog')->delete($form['content_file']);
            Storage::disk('blog')->put($form['content_file'], $encrypt_content);

            unset($form['blog_content']);
            $result = $this->model->where('id', $id)->update($form);
        }

        if (!$result) {
            return $this->jsonReturn(201, '保存失败');
        }
        return $this->jsonReturn(200, '保存成功');
    }

    public function deleteOne(Request $request)
    {
        $id = $request->json('id');
        $result = $this->model->where('id', $id)->delete();
        if (!$result) {
            return $this->jsonReturn(201, '删除失败');
        }
        return $this->jsonReturn(200, '删除成功！');
    }

    public function imgUpload(Request $request)
    {
        $file = $request->file('banner');
        $ext = $file->getClientOriginalExtension();
        $path = $file->getRealPath();
        $fileName = uniqid() . '.' . $ext;
        $result = Storage::disk('banner')->put($fileName, file_get_contents($path));
        if (!$result) {
            return $this->jsonReturn(201, '上传失败');
        }

        $filePath = asset('storage/banner/' . $fileName);

        $this->model->create(['banner_url' => $filePath]);

        return $this->jsonReturn(200, 'succeed', $filePath);
    }

}
