<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Index extends Controller
{
    public function index(Request $request)
    {
        $blogTotal = Blog::where('status', 1)->count();
        $blogReadSum = Blog::where('status', 1)->sum('view_sum');
        return view('welcome', compact('blogTotal', 'blogReadSum'));
    }
}
