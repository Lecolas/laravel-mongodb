<?php

namespace App\Services\MongoDBLogServices;

use App\Services\MongoDBLogServices\Authenticate\Connect;
use App\Services\MongoDBLogServices\Queue\Queue;

class MongoLogServices
{
    protected $connection;
    protected $client;

    public function __construct()
    {
        $this->connection = (new connect())->connection();
        $this->client = new Queue($this->connection);
    }

    public function __call(string $name, array $arguments)
    {
        // TODO: Implement __call() method.
        return $this->client->{$name}(...$arguments);
    }



}
