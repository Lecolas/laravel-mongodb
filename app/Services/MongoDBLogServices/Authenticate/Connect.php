<?php

namespace App\Services\MongoDBLogServices\Authenticate;

use MongoDB\Driver\Manager;

class Connect
{
    protected $host;
    protected $port;
    protected $bulk;
    protected $mongo;
    protected $db;

    public function connection()
    {
        $this->host = config('services.mongoDB.host');
        $this->port = config('services.mongoDB.port');
        $this->db = config('services.mongoDB.database');

        try {
            $connection_str = "mongodb://{$this->host}:{$this->port}";
            !empty($db_name) && $connection_str .= "/{$this->db}";
            return new Manager($connection_str);
        } catch (\Exception $e) {
            print_r($e->getMessage());die;
        }
    }
}
