<?php
namespace App\Services\MongoDBLogServices\Queue;

use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Query;
use MongoDB\Driver\WriteConcern;
use function PHPUnit\Framework\throwException;

class Queue
{
    protected $db;
    protected $bulk;
    protected $collection;
    protected $connection;
    static $filter = [];
    static $options = [];

    public function __construct($connect)
    {
        $this->bulk = new BulkWrite();
        $this->connection = $connect;
        $this->collection = 'Log';
    }

    /**
     * @param mixed ...$args
     * @return $this
     * @throws \Exception
     */
    public function where(...$args)
    {
        if (count($args) == 2) {
            if (is_string($args[0]) && is_string($args[1])) {
                self::$filter[$args[0]] = $args[1];
            }
        } else {
            foreach ($args as $key => $val) {
                if (count($val) == 1) {
                    foreach ($val as $subKey => $subVal) {
                        self::$filter[$subKey] = $subVal;
                    }
                }
                if (count($val) == 3) {
                    self::$filter[$val[0]] = [$this->symbolTransition($val[1]), $val[2]];
                }
            }
        }
        return $this;
    }

    public function orWhere($field, $operation = null, $value = null)
    {
        $orArr = [];
        if (is_array($field)) {
            foreach ($field as $key => $val) {
                if (!is_array($val)) {
                    $orArr[$key] = $val;
                }
                if (count($val) == 3) {
                    $orArr[$val[0]] = [$this->symbolTransition($val[1]), $val[2]];
                }
            }
        }
        self::$filter['$or'] = $orArr;
    }

    public function select(...$args)
    {
        foreach ($args as $key => $val) {
            if (is_array($val)) {
                self::$options['projection'] = $val;
            } else {
                self::$options['projection'][] = $val;
            }
        }
        return $this;
    }

    public function sort($name, $ascending = 'asc')
    {
        $algor = ['acs' => 1, 'desc' => -1];
        self::$options['sort'] =  array_merge(self::$options['sort'], [$name => $algor[strtolower($ascending)]]);
        return $this;
    }

    public function get()
    {
        $query = new Query(self::$filter, self::$options);
        $result = $this->connection->executeQuery($this->setCollection(), $query);
        return $this->object_array($result);
    }

    public function update(array $updateArgs)
    {
        $update = [];
        if (count($updateArgs) == 2) {
            $update[$updateArgs[0]] = $updateArgs[1];
        } else {
            $update = $updateArgs;
        }

        $this->bulker($update, true);
        return $this->write();
    }

    public function createOrUpdate(array $update)
    {
        $this->bulker($update, true, true);
        return $this->write();
    }

    public function insert(array $data)
    {
        $this->bulk->insert($data);
        return $this->write();
    }

    public function delete()
    {
        $this->bulk->delete(self::$filter);
        return $this->write();
    }

    private function bulker($set, $multiple = false, $upsert = false)
    {
        $this->bulk->update(
            self::$filter,
            ['$set' => $set],
            ['multi' => $multiple, 'upsert' => $upsert]
        );
    }

    private function setCollection(): string
    {
        return config('services.mongoDB.database') . '.' . $this->collection;
    }

    private function symbolTransition(string $symbol)
    {
        $symbolCol = [
            '<' => 'lt',
            '<=' => 'lte',
            '>' => 'gt',
            '>=' => 'gte',
            '!=' => 'ne'
        ];
        if (!isset($symbolCol[$symbol])) {
            throw new \Exception('Unsupport Comparison Symbol' . $symbol, 500);
        }
        return $symbolCol[$symbol];
    }

    private function object_array($array)
    {
        return json_decode(json_encode(iterator_to_array($array)), true);
    }

    private function write()
    {
        $writeConcern = new WriteConcern(WriteConcern::MAJORITY, 1000);
        return $this->connection->executeBulkWrite($this->setCollection(), $this->bulk, $writeConcern);
    }
}
