<?php
namespace App\Services\Membership;

use Illuminate\Support\Facades\Validator;

class MembershipValidations
{
    public function makeValidation(array $data): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($data, $this->requiredField(), $this->fieldMessage());
    }

    private function requiredField(): array
    {
        return [
            'member_name' => 'required',
            'phone' => 'required|max:11',
            'email' => 'required'
        ];
    }

    private function fieldMessage(): array
    {
        return [
            'member_name.required' => 'Member name should not be empty',
            'phone.required' => 'Member phone number should not be empty',
            'phone.max' => 'Phone number should not larger than 11 numbers',
            'email.required' => 'Email should not be empty'
        ];
    }
}
