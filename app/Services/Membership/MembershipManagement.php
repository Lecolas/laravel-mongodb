<?php
namespace App\Services\Membership;


use App\Models\Membership;
use Illuminate\Support\Facades\Request;

class MembershipManagement
{
    public function __construct()
    {

    }

    public function createMember(Request $request): \Illuminate\Http\JsonResponse
    {
        $only = ['member_name', 'phone', 'email'];
        $formData = $request->only($only);
        $validation = (new MembershipValidations())->makeValidation($formData);

        if ($validation->fails()) {
            return response()->json(['code' => 51000, 'msg' => $validation->errors()->first(), 'data' => '']);
        }

        $result = Membership::create($formData);
        if (!$result) {
            return response()->json(['code' => 51000, 'msg' => 'Create failed', 'data' => '']);
        }
        return response()->json(['code' => 50000, 'msg' => 'Create successfully', 'data' => '']);
    }

    public function membershipValidation(string $phone)
    {
        $row = Membership::where('phone', $phone)->first();
        if (!$row) {
            return response()->json(['code' => 51000, 'msg' => "Member {$phone} does not exist", 'data' => '']);
        }

        if ($row->valid_date > date('Y-m-d H:i:s')) {
            return true;
        }

        $row->status = 4;
        $row->save();
        return false;
    }
}
