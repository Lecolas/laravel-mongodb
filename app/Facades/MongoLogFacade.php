<?php
namespace App\Facades;

/**
 * @method static void insert(array $data)
 * @method static void where(array $args)
 *
 * @see \App\Services\MongoDBLogServices\Queue\Queue
 */

use Illuminate\Support\Facades\Facade;
class MongoLogFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MongoLog';
    }
}
