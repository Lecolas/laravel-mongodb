<?php

namespace App\Providers;

use App\Services\MongoDBLogServices\MongoLogServices;
use Illuminate\Support\ServiceProvider;

class MongoLogServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('MongoLog', function () {
            return new MongoLogServices();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
